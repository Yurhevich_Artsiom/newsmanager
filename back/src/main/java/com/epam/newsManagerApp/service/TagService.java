package com.epam.newsManagerApp.service;

import com.epam.newsManagerApp.domain.Author;
import com.epam.newsManagerApp.domain.Tag;
import com.epam.newsManagerApp.service.exception.ServiceException;

import java.util.List;

/**
 * Implementation of this interface allows to process
 * set of operations with tags
 */
public interface TagService {

    /**
     * Transfers Tag object to DAO to check if the tag
     * exists, if not insert a new row
     *
     * @param tag instance to create
     * @return tag's id
     * @throws ServiceException
     */

    Long getOrCreate(Tag tag) throws ServiceException;

    /**
     * Transfers List of Tag objects to DAO
     * to check each tag if it
     * exists, if not insert a new row
     *
     * @param tags List of tags to create
     * @return tags' ids
     * @throws ServiceException
     */

    List<Long> getOrCreate(List<Tag> tags) throws ServiceException;

    /**
     * Fetches all tags
     *
     * @return List of Tag objects
     * @throws ServiceException
     */

    List<Tag> getAll() throws ServiceException;

    /**
     * Edits tag, transfering
     * the object instance to DAO to update
     *
     * @param tag instance to edit
     * @throws ServiceException
     */

    void editTag(Tag tag) throws ServiceException;

    /**
     * Transfers tag' id to DAO to delete a Tag
     * record with requested id
     *
     * @param tagId tag's id
     * @throws ServiceException
     */

    void deleteTag(Long tagId) throws ServiceException;

    /**
     * Fetches all tags, associated
     * with definite news, by news' id
     *
     * @param newsId news's id
     * @return List of Tag objects
     * @throws ServiceException
     */

    List<Tag> getNewsTags(Long newsId) throws ServiceException;

    /**
     * Unlink definite tag from all associations
     * with news
     *
     * @param tagId tag's id
     * @throws ServiceException
     */

    void unlinkTagFromAllNews(Long tagId) throws ServiceException;

    boolean existAll(List<Tag> tags) throws ServiceException;

    List<Long> getId(List<Tag> tags) throws ServiceException;
    
    List<Tag> get(Integer from, Integer count) throws ServiceException;

    Long getId(Tag tag) throws ServiceException;

    int countAll() throws ServiceException;

    /**
     * Transfers List of Tag objects to DAO
     * to check each tag if it
     * exists, if not insert a new row
     *
     * @param tags List of tags to create
     * @return tags' ids
     * @throws ServiceException
     */

    Long create(Tag tag) throws ServiceException;

}
