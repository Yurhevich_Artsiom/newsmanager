package com.epam.newsManagerApp.service.impl;

import com.epam.newsManagerApp.dao.AuthorDAO;
import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.domain.Author;
import com.epam.newsManagerApp.service.AuthorService;
import com.epam.newsManagerApp.service.exception.ServiceException;
import com.epam.newsManagerApp.transfer.search.AuthorSearchCriteria;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * {@inheritDoc}
 * <p>
 * Service class that provides some
 * features set to work with Author
 */

@Service
public class AuthorServiceImpl implements AuthorService {

    private static final Logger LOGGER = Logger.getLogger(AuthorServiceImpl.class);

    @Autowired
    private AuthorDAO authDao;


    @Override
    public List<Author> getAll() throws ServiceException {

        try {
            return authDao.fetchAll();
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Long getOrCreate(Author author) throws ServiceException {

        Long id;

        if (author == null) {
            throw new IllegalArgumentException("Author is null");
        }

        try {
            id = authDao.exist(author);

            if (id == null) {
                id = authDao.insert(author);
            }
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

        return id;
    }

    @Override
    public void editAuthor(Author author) throws ServiceException {

        if (author == null) {
            throw new IllegalArgumentException("Author is null");
        }

        try {
            authDao.update(author);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void setExpired(Author author) throws ServiceException {
        if (author == null) {
            throw new IllegalArgumentException("Author is null");
        }

        try {
            authDao.setExpirationDate(author);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Author getNewsAuthor(Long newsId) throws ServiceException {

        if (newsId == null) {
            throw new IllegalArgumentException("News id is null");
        }

        try {
            return authDao.getNewsAuthor(newsId);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

    }

    @Override
    public Long getId(Author author) throws ServiceException {

        Long id;

        if (author == null) {
            throw new IllegalArgumentException("Author is null");
        }

        try {
            id = authDao.exist(author);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

        return id;
    }

    @Override
    public List<Author> get(Integer from, Integer count) throws ServiceException {


        if (from == null || count == null) {
            throw new IllegalArgumentException("From or count is null");
        }

        try {
            return authDao.get(from, count);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public int countAll() throws ServiceException {
        try {
            return authDao.countAll();
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }
}
