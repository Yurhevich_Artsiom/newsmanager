package com.epam.newsManagerApp.service.impl;

import com.epam.newsManagerApp.dao.NewsDAO;
import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.domain.News;
import com.epam.newsManagerApp.service.NewsService;
import com.epam.newsManagerApp.service.exception.ServiceException;
import com.epam.newsManagerApp.transfer.search.NewsSearchCriteria;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * {@inheritDoc}
 * <p>
 * Service class that provides some features set to work with News
 */

@Service
public class NewsServiceImpl implements NewsService {

    private static final Logger LOGGER = Logger.getLogger(NewsServiceImpl.class);

    @Autowired
    private NewsDAO newsDAO;

    @Override
    public List<News> getNews(NewsSearchCriteria searchCriteria) throws ServiceException {

        if (searchCriteria == null) {
            throw new IllegalArgumentException("Search criteria object is null");
        }
        try {
            return newsDAO.getNews(searchCriteria);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public News getNews(Long newsId) throws ServiceException {

        if (newsId == null) {
            throw new IllegalArgumentException("News id is null");
        }
        try {
            return newsDAO.fetch(newsId);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Long addNews(News news) throws ServiceException {

        if (news == null) {
            throw new IllegalArgumentException("News object is null");
        }
        try {
            return newsDAO.insert(news);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void linkAuthor(Long authId, Long newsId) throws ServiceException {

        if (authId == null) {
            throw new IllegalArgumentException("Author id is null");
        }
        if (newsId == null) {
            throw new IllegalArgumentException("News id is null");
        }
        try {
            newsDAO.linkToAuthor(newsId, authId);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void linkTags(Long newsId, List<Long> tagsId) throws ServiceException {

        if (newsId == null) {
            throw new IllegalArgumentException("News id is null");
        }
        if (tagsId == null) {
            throw new IllegalArgumentException("Author id is null");
        }
        try {
            newsDAO.linkToTags(newsId, tagsId);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void unlinkAuthor(Long newsId) throws ServiceException {

        if (newsId == null) {
            throw new IllegalArgumentException("News id is null");
        }

        try {
            newsDAO.unlinkAuthor(newsId);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void unlinkTags(Long newsId) throws ServiceException {

        if (newsId == null) {
            throw new IllegalArgumentException("News id is null");
        }

        try {
            newsDAO.unlinkTags(newsId);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void editNews(News news) throws ServiceException {
        if (news == null) {
            throw new IllegalArgumentException("News object is null");
        }

        try {
            newsDAO.update(news);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteNews(Long newsId) throws ServiceException {
        if (newsId == null) {
            throw new IllegalArgumentException("News id is null");
        }

        try {
            newsDAO.delete(newsId);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public int countNews() throws ServiceException {
        try {
            return newsDAO.countNews();
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public int countNews(NewsSearchCriteria searchCriteria) throws ServiceException {
    	
    	 if (searchCriteria == null) {
             throw new IllegalArgumentException("Search criteria object is null");
         }
    	 
        try {
            return newsDAO.countNews(searchCriteria);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Long[] nextPrevNewsNumber(Long newsId, NewsSearchCriteria criteria) throws ServiceException {
    	
        if (newsId == null) {
            throw new IllegalArgumentException("News id is null");
        }
        
        try {

            Long newsOrderNum = newsDAO.getNewsRowNum(newsId, criteria);

            if(newsOrderNum==null){
                throw new IllegalArgumentException("News doesn't exist");
            }

            Long[] prevNextOrderNum = newsDAO.getNextPrevRowNum(newsOrderNum, criteria);

            return prevNextOrderNum;

        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

	@Override
	public Long getIdByOrderNum(NewsSearchCriteria searchCriteria, Long orderNum) throws ServiceException {
		
		if (searchCriteria == null) {
            throw new IllegalArgumentException("Search criteria object is null");
        }
   	 
		if(orderNum == null){
		     throw new IllegalArgumentException("Order num is null");
		}
		
       try {
           return newsDAO.getIdByOrderNum(searchCriteria, orderNum);
       } catch (DAOException e) {
           LOGGER.error(e);
           throw new ServiceException(e);
       }
	}




}
