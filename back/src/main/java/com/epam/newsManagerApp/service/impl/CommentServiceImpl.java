package com.epam.newsManagerApp.service.impl;

import com.epam.newsManagerApp.dao.CommentDAO;
import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.domain.Comment;
import com.epam.newsManagerApp.service.CommentService;
import com.epam.newsManagerApp.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * {@inheritDoc}
 * <p>
 * Service class that provides some
 * features set to work with Comment
 */

@Service
public class CommentServiceImpl implements CommentService {

    private static final Logger LOGGER = Logger.getLogger(CommentServiceImpl.class);

    @Autowired
    private CommentDAO commentDAO;

    @Override
    public Long addComment(Comment comment) throws ServiceException {

        if (comment == null) {
            throw new IllegalArgumentException("Comment is null");
        }

        try {
            return commentDAO.insert(comment);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

    }


    @Override
    public void deleteComment(Long commentId) throws ServiceException {

        if (commentId == null) {
            throw new IllegalArgumentException("Comment id is null");
        }

        try {
            commentDAO.delete(commentId);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

    }


    @Override
    public List<Comment> getNewsComments(Long newsId) throws ServiceException {

        if (newsId == null) {
            throw new IllegalArgumentException("News id is null");
        }

        try {
            return commentDAO.getNewsComments(newsId);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

    }

    @Override
    public void deleteNewsComments(Long newsId) throws ServiceException {

        if (newsId == null) {
            throw new IllegalArgumentException("News id is null");
        }

        try {
            commentDAO.deleteNewsComments(newsId);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }

    }
}
