package com.epam.newsManagerApp.service;

import com.epam.newsManagerApp.service.exception.ServiceException;
import com.epam.newsManagerApp.transfer.NewsTransferObject;
import com.epam.newsManagerApp.transfer.search.NewsSearchCriteria;

import java.util.List;

/**
 * Implementation of this interface allows to process set of
 * transactional operations with news transfer objects
 */

public interface NewsTOService {

    /**
     * Fetch an instance of transfer object, which includes
     * news, tags, authors and comments. Searching executes by expected
     * news' id
     *
     * @param id news' id
     * @return news transfer object
     * @throws ServiceException
     */

    NewsTransferObject getNewsTransferObject(Long id) throws ServiceException;


    /**
     * Fetch a list of transfer objects, each obj includes
     * news, tags, authors and comments. Searching executes by
     * NewsSearchCriteria object, which contains
     * tags, author and boolean value, representing sorting
     * news by number of their comments.
     *
     * @param criteria NewsSearchCriteria object to find
     *                 news with expected parameters
     * @return a list of news transfer objects
     * @throws ServiceException
     */

    List<NewsTransferObject> getNewsTransferObject(NewsSearchCriteria criteria) throws ServiceException;

    /**
     * Processes transactional execution of inserting news dto,
     * which contains inserting news, tags if not exist,
     * author if not exists and associating them
     *
     * @param newsTO transfer object to add, which includes news,
     *               tags, comments, author
     * @throws ServiceException
     */

    Long addNews(NewsTransferObject newsTO) throws ServiceException;


    /**
     * Processes transactional edition of existing news,
     * which contains updating news, unlinkung author,
     * association with new author, unlinking tags and
     * association with new ones
     *
     * @param newsTO transfer object to update, which includes news,
     *               tags, author
     * @throws ServiceException
     */
    void editNews(NewsTransferObject newsTO) throws ServiceException;


    /**
     * Processes transactional deleting of existing news,
     * which contains  unlinkung author, unlinking tags and
     * then deleting news
     *
     * @param newsId id of the news to delete
     * @throws ServiceException
     */
    void deleteNews(Long newsId) throws ServiceException;


}
