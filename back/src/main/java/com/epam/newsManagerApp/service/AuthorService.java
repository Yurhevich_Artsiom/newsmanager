package com.epam.newsManagerApp.service;

import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.domain.Author;
import com.epam.newsManagerApp.service.exception.ServiceException;
import com.epam.newsManagerApp.transfer.search.AuthorSearchCriteria;

import java.util.List;

/**
 * Implementation of this interface allows to process
 * set of operations with authors
 */
public interface AuthorService {

    /**
     * Transfers Author object to DAO to check if the author
     * exists, if not insert a new row
     *
     * @param author instance to create
     * @return author's id
     * @throws ServiceException
     */

    Long getOrCreate(Author author) throws ServiceException;

    /**
     * Edits Author object, transfering
     * the instance to DAO to update
     *
     * @param author instance to edit
     * @throws ServiceException
     */

    void editAuthor(Author author) throws ServiceException;

    /**
     * Sets expiration date to the author
     *
     * @param author to expire
     * @throws ServiceException
     */

    void setExpired(Author author) throws ServiceException;

    /**
     * Fetches all authors
     *
     * @return List of Author objects
     * @throws ServiceException
     */

    List<Author> getAll() throws ServiceException;


    /**
     * Fetches the author of the news
     * with requested id,
     * null if this news' id doesn't exist in db
     *
     * @param newsId id of the author
     * @return The author of the news
     * @throws ServiceException
     */

    Author getNewsAuthor(Long newsId) throws ServiceException;

    Long getId(Author author) throws ServiceException;

    List<Author> get(Integer from, Integer count) throws ServiceException;

    int countAll() throws ServiceException;
}
