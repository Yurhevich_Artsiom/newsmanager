package com.epam.newsManagerApp.service.impl;

import com.epam.newsManagerApp.domain.News;
import com.epam.newsManagerApp.service.*;
import com.epam.newsManagerApp.service.exception.ServiceException;
import com.epam.newsManagerApp.transfer.NewsTransferObject;
import com.epam.newsManagerApp.transfer.search.NewsSearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * {@inheritDoc}
 * <p>
 * Service class that provides some
 * features set to work with NewsTransferObject
 */

@Service
public class NewsTOServiceImpl implements NewsTOService {

    @Autowired
    private AuthorService authorService;

    @Autowired
    private NewsService newsService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private TagService tagService;


    @Override
    public NewsTransferObject getNewsTransferObject(Long id) throws ServiceException {

        NewsTransferObject newsTO;

        News news = newsService.getNews(id);

        if (news == null) {
            return null;
        }

        newsTO = new NewsTransferObject();

        newsTO.setNews(news);
        newsTO.setAuthor(authorService.getNewsAuthor(id));
        newsTO.setTags(tagService.getNewsTags(id));
        newsTO.setComments(commentService.getNewsComments(id));

        return newsTO;
    }

    @Override
    public List<NewsTransferObject> getNewsTransferObject(NewsSearchCriteria searchCriteria) throws ServiceException {

        List<NewsTransferObject> newsTOList;
        NewsTransferObject newsTO;

        if (searchCriteria == null) {
            throw new IllegalArgumentException("Criteria is null");
        }

        newsTOList = new ArrayList<>();

        List<News> newsList = newsService.getNews(searchCriteria);

        for (News news : newsList) {

            newsTO = new NewsTransferObject();

            newsTO.setNews(news);
            newsTO.setAuthor(authorService.getNewsAuthor(news.getId()));
            newsTO.setComments(commentService.getNewsComments(news.getId()));
            newsTO.setTags(tagService.getNewsTags(news.getId()));
            newsTOList.add(newsTO);
        }

        return newsTOList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long addNews(NewsTransferObject newsTO) throws ServiceException {

        if (newsTO == null) {
            throw new IllegalArgumentException("News transfer object is null");
        }

        if (newsTO.getNews().getCreationDate() == null) {
            newsTO.getNews().setCreationDate(LocalDateTime.now());
        }

        Long newsId = newsService.addNews(newsTO.getNews());

        Long authorId = authorService.getId(newsTO.getAuthor());


        newsService.linkAuthor(authorId, newsId);

        List<Long> tagsId = tagService.getId(newsTO.getTags());

        newsService.linkTags(newsId, tagsId);

        return newsId;

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void editNews(NewsTransferObject newsTO) throws ServiceException {

        if (newsTO == null) {
            throw new IllegalArgumentException("News transfer object is null");
        }

        newsTO.getNews().setModificationDate(LocalDateTime.now());

        Long newsId = newsTO.getNews().getId();

        newsService.editNews(newsTO.getNews());

        newsService.unlinkAuthor(newsId);

        Long authorId = authorService.getId(newsTO.getAuthor());

        newsService.linkAuthor(authorId, newsId);

        newsService.unlinkTags(newsId);

        List<Long> tagsId = tagService.getId(newsTO.getTags());

        newsService.linkTags(newsId, tagsId);

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteNews(Long newsId) throws ServiceException {

        if (newsId == null) {
            throw new IllegalArgumentException("News id is null");
        }

        newsService.unlinkAuthor(newsId);

        newsService.unlinkTags(newsId);

        commentService.deleteNewsComments(newsId);

        newsService.deleteNews(newsId);

    }




}
