package com.epam.newsManagerApp.service.impl;

import com.epam.newsManagerApp.dao.TagDAO;
import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.domain.Tag;
import com.epam.newsManagerApp.service.TagService;
import com.epam.newsManagerApp.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * {@inheritDoc}
 * <p>
 * Service class that provides some
 * features set to work with Tag
 */

@Service
public class TagServiceImpl implements TagService {

    private static final Logger LOGGER = Logger.getLogger(TagServiceImpl.class);

    @Autowired
    private TagDAO tagDAO;


    @Override
    public Long getOrCreate(Tag tag) throws ServiceException {

        Long idTag;

        if (tag == null) {
            throw new IllegalArgumentException("Tag is null");
        }

        try {

            idTag = tagDAO.exist(tag);

            if (idTag == null) {
                idTag = tagDAO.insert(tag);
            }

        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return idTag;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<Long> getOrCreate(List<Tag> tags) throws ServiceException {

        if (tags == null) {
            throw new IllegalArgumentException("Tag is null");
        }

        ArrayList<Long> tagIds = new ArrayList<>();

        try {
            for (Tag tag : tags) {
                Long existedTag = tagDAO.exist(tag);
                if (existedTag == null) {
                    tagIds.add(tagDAO.insert(tag));
                } else {
                    tagIds.add(existedTag);
                }
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return tagIds;
    }

    @Override
    public List<Tag> getAll() throws ServiceException {
        try {
            return tagDAO.getAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }


    @Override
    public void editTag(Tag tag) throws ServiceException {

        if (tag == null) {
            throw new IllegalArgumentException("Tag is null");
        }

        try {
            tagDAO.update(tag);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteTag(Long tagId) throws ServiceException {

        if (tagId == null) {
            throw new IllegalArgumentException("Tag id is null");
        }

        try {
            unlinkTagFromAllNews(tagId);
            tagDAO.delete(tagId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Tag> getNewsTags(Long newsId) throws ServiceException {
        if (newsId == null) {
            throw new IllegalArgumentException("News id is null");
        }

        try {
            return tagDAO.fetchByNewsId(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

    }


    @Override
    public void unlinkTagFromAllNews(Long tagId) throws ServiceException {
        if (tagId == null) {
            throw new IllegalArgumentException("Tag id is null");
        }

        try {
            tagDAO.unlinkTagFromAllNews(tagId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean existAll(List<Tag> tags) throws ServiceException {
        try {
            for (Tag tag : tags) {
                if (tagDAO.exist(tag) == null) {
                    return false;
                }
            }

            return true;

        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    // tag text == null?
    @Override
    public List<Long> getId(List<Tag> tags) throws ServiceException {
        if (tags == null) {
            throw new IllegalArgumentException("Tag list is null");
        }

        ArrayList<Long> tagIds = new ArrayList<>();

        try {
            for (Tag tag : tags) {
                Long existedTag = tagDAO.exist(tag);
                tagIds.add(existedTag);
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return tagIds;
    }


    @Override
    public int countAll() throws ServiceException {
        try {
            return tagDAO.countAll();
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Long create(Tag tag) throws ServiceException {

        try {
            return tagDAO.insert(tag);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Long getId(Tag tag) throws ServiceException {

        if (tag == null) {
            throw new IllegalArgumentException("Tag is null");
        }

        if (tag.getTag() == null) {
            throw new IllegalArgumentException("Tag text is null");
        }

        try {
            return tagDAO.exist(tag);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Tag> get(Integer from, Integer count) throws ServiceException {

        if (from == null || count == null) {
            throw new IllegalArgumentException("From or count is null");
        }

        try {
            return tagDAO.get(from, count);
        } catch (DAOException e) {
            LOGGER.error(e);
            throw new ServiceException(e);
        }
    }


}
