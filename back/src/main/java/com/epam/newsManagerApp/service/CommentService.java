package com.epam.newsManagerApp.service;

import com.epam.newsManagerApp.domain.Comment;
import com.epam.newsManagerApp.service.exception.ServiceException;

import java.util.List;

/**
 * Implementation of this interface allows to process
 * set of operations with comments
 */
public interface CommentService {

    /**
     * Transfers Comment object to DAO to insert a new record
     *
     * @param comment to create
     * @return created comment's id
     * @throws ServiceException
     */

    Long addComment(Comment comment) throws ServiceException;

    /**
     * Transfers Comment's id to DAO to delete a record
     * with requested id
     *
     * @param commentId comment's id
     * @throws ServiceException
     */

    void deleteComment(Long commentId) throws ServiceException;


    /**
     * Fetches all comments, associated
     * with definite news, by news' id
     *
     * @param newsId news's id
     * @return List of Comment objects
     * @throws ServiceException
     */

    List<Comment> getNewsComments(Long newsId) throws ServiceException;

    /**
     * Deletes all comments, associated
     * with definite news, by news' id
     *
     * @param newsId news's id
     * @throws ServiceException
     */

    void deleteNewsComments(Long newsId) throws ServiceException;

}
