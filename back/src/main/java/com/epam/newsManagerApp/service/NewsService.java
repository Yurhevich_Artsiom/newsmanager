package com.epam.newsManagerApp.service;

import com.epam.newsManagerApp.domain.News;
import com.epam.newsManagerApp.service.exception.ServiceException;
import com.epam.newsManagerApp.transfer.search.NewsSearchCriteria;

import java.util.List;

/**
 * Implementation of this interface allows to process
 * set of operations with news
 */
public interface NewsService {

    /**
     * Finds a list of news, associated with expected parameters in
     * search criteria object, wich includes
     * tags, author and boolean value, representing sorting
     * news by number of their comments.
     *
     * @param criteria NewsSearchCriteria object to find
     *                 news with expected parameters
     * @return a list of appropriate news
     * @throws ServiceException
     */

    List<News> getNews(NewsSearchCriteria criteria) throws ServiceException;

    /**
     * Fetches the news with definite id,
     * null if the id doesn't exist
     *
     * @param newsId news' id
     * @return appropriate News object
     * @throws ServiceException
     */

    News getNews(Long newsId) throws ServiceException;

    /**
     * Transfers News object to DAO to insert a new record
     *
     * @param news to create
     * @return created news' id
     * @throws ServiceException
     */

    Long addNews(News news) throws ServiceException;

    /**
     * Counts all existed news in database
     *
     * @return count of news
     * @throws ServiceException
     */

    int countNews() throws ServiceException;

    /**
     * Associates an author with definite news,
     * by their ids
     *
     * @param newsId news' id
     * @param authId author's id
     * @throws ServiceException
     */


    void linkAuthor(Long newsId, Long authId) throws ServiceException;

    /**
     * Associates list of tags with definite news,
     * by their ids
     *
     * @param newsId news' id
     * @param tagsId list of tags' id
     * @throws ServiceException
     */

    void linkTags(Long newsId, List<Long> tagsId) throws ServiceException;

    /**
     * Unlink the associated author from the
     * definite news
     *
     * @param newsId news' id
     * @throws ServiceException
     */

    void unlinkAuthor(Long newsId) throws ServiceException;

    /**
     * Unlink the associated tags from the
     * definite news
     *
     * @param newsId news' id
     * @throws ServiceException
     */

    void unlinkTags(Long newsId) throws ServiceException;

    /**
     * Edits news, transfering
     * the object instance to DAO to update
     *
     * @param news instance to edit
     * @throws ServiceException
     */

    void editNews(News news) throws ServiceException;

    /**
     * Transfers news' id to DAO to delete a News
     * record with requested id
     *
     * @param newsId news id
     * @throws ServiceException
     */

    void deleteNews(Long newsId) throws ServiceException;


    int countNews(NewsSearchCriteria criteria) throws ServiceException;

    Long[] nextPrevNewsNumber(Long newsId, NewsSearchCriteria criteria) throws ServiceException;

	Long getIdByOrderNum(NewsSearchCriteria criteria, Long orderNum) throws ServiceException;

	
}
