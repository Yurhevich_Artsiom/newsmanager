package com.epam.newsManagerApp.dao.exception;

/**
 * Created by Comp on 21.06.2016.
 */
public class DAOException extends Exception {

    public DAOException() {
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOException(String message) {
        super(message);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }
}
