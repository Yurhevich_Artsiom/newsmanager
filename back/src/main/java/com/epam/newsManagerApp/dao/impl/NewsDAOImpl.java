package com.epam.newsManagerApp.dao.impl;

import com.epam.newsManagerApp.dao.NewsDAO;
import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.dao.query.QueryGenerator;
import com.epam.newsManagerApp.domain.News;
import com.epam.newsManagerApp.transfer.search.NewsSearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * {@inheritDoc}
 * <p>
 * Class provides some features set to work with News entities in database
 */

@Component
public class NewsDAOImpl implements NewsDAO {

    private final String SQL_INSERT_NEWS = "INSERT INTO NEWS(TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES(?, ?, ?, ?, ?)";
    private final String SQL_UPDATE_NEWS = "UPDATE NEWS SET TITLE = ?, SHORT_TEXT = ?, FULL_TEXT = ?, MODIFICATION_DATE = ? WHERE NEWS_ID = ?";
    private final String SQL_FETCH_BY_ID = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM NEWS WHERE NEWS_ID = ?";
    private final String SQL_DELETE_NEWS = "DELETE FROM NEWS WHERE NEWS_ID = ?";
    private final String SQL_COUNT_ALL_NEWS = "SELECT COUNT(*) FROM NEWS";
    private final String SQL_LINK_TAG = "INSERT INTO NEWS_TAGS(NEWS_ID, TAG_ID) VALUES(?, ?)";
    private final String SQL_LINK_AUTHOR = "INSERT INTO NEWS_AUTHORS(NEWS_ID, AUTHOR_ID) VALUES(?, ?)";
    private final String SQL_UNLINK_AUTHOR = "DELETE FROM NEWS_AUTHORS WHERE NEWS_AUTHORS.NEWS_ID = ?";
    private final String SQL_UNLINK_TAGS = "DELETE FROM NEWS_TAGS WHERE NEWS_TAGS.NEWS_ID = ?";
   
    @Autowired
    private DataSource dataSource;

    @Autowired
    private QueryGenerator generator;
    
    @Override
    public Long insert(News news) throws DAOException {
        Long id = null;
        Connection con = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = con.prepareStatement(SQL_INSERT_NEWS, new String[]{"NEWS_ID"})) {

            ps.setString(1, news.getTitle());
            ps.setString(2, news.getShortText());
            ps.setString(3, news.getFullText());
            ps.setTimestamp(4, Timestamp.valueOf(news.getCreationDate()));
            ps.setTimestamp(5, Timestamp.valueOf(news.getCreationDate()));

            ps.executeUpdate();

            try (ResultSet rs = ps.getGeneratedKeys()) {

                if (rs.next()) {
                    id = rs.getLong(1);
                }
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }
        return id;

    }

    @Override
    public News fetch(Long id) throws DAOException {
        News news = null;
        Connection con = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = con.prepareStatement(SQL_FETCH_BY_ID)) {

            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    news = initializeFromResultSet(rs);
                }
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }
        return news;
    }

    @Override
    public void update(News news) throws DAOException {

        Connection con = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement ps = con.prepareStatement(SQL_UPDATE_NEWS)) {

            ps.setString(1, news.getTitle());
            ps.setString(2, news.getShortText());
            ps.setString(3, news.getFullText());
            ps.setTimestamp(4, Timestamp.valueOf(news.getModificationDate()));
            ps.setLong(5, news.getId());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }

    }

    @Override
    public void delete(Long newsId) throws DAOException {

        Connection con = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement ps = con.prepareStatement(SQL_DELETE_NEWS)) {

            ps.setLong(1, newsId);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }

    }

    @Override
    public int countNews() throws DAOException {
        int newsCount = 0;
        Connection con = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement ps = con.prepareStatement(SQL_COUNT_ALL_NEWS); ResultSet rs = ps.executeQuery()) {

            if (rs.next()) {
                newsCount = rs.getInt(1);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }
        return newsCount;
    }

    @Override
    public int countNews(NewsSearchCriteria criteria) throws DAOException {
        Connection con = DataSourceUtils.getConnection(dataSource);

        String query = generator.generateCountNewsQuery(criteria);

        int newsCount = 0;

        try (PreparedStatement ps = con.prepareStatement(query); ResultSet rs = ps.executeQuery()) {

            if (rs.next()) {
                newsCount = rs.getInt(1);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }
        return newsCount;
    }

    @Override
    public void linkToAuthor(Long newsId, Long authId) throws DAOException {

        Connection con = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement ps = con.prepareStatement(SQL_LINK_AUTHOR)) {

            ps.setLong(1, newsId);
            ps.setLong(2, authId);
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }
    }

    @Override
    public List<News> getNews(NewsSearchCriteria searchCriteria) throws DAOException {

        Connection con = DataSourceUtils.getConnection(dataSource);

        String query = generator.generateSelectNewsQuery(searchCriteria);

        List<News> listNews;

        try (PreparedStatement ps = con.prepareStatement(query); ResultSet rs = ps.executeQuery()) {

            listNews = new ArrayList<>();

            while (rs.next()) {
                listNews.add(initializeFromResultSet(rs));
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }
        return listNews;

    }

    @Override
    public void linkToTags(Long newsId, List<Long> tagsId) throws DAOException {

        Connection con = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement ps = con.prepareStatement(SQL_LINK_TAG)) {

            for (Long tagId : tagsId) {
                ps.setLong(1, newsId);
                ps.setLong(2, tagId);
                ps.executeUpdate();
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }
    }

    @Override
    public void unlinkAuthor(Long newsId) throws DAOException {

        Connection con = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement ps = con.prepareStatement(SQL_UNLINK_AUTHOR)) {

            ps.setLong(1, newsId);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }
    }

    @Override
    public void unlinkTags(Long newsId) throws DAOException {

        Connection con = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement ps = con.prepareStatement(SQL_UNLINK_TAGS)) {

            ps.setLong(1, newsId);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }
    }

    private News initializeFromResultSet(ResultSet rs) throws SQLException {
        News news = new News();
        news.setId(rs.getLong(1));
        news.setTitle(rs.getString(2));
        news.setShortText(rs.getString(3));
        news.setFullText(rs.getString(4));
        news.setCreationDate(rs.getTimestamp(5).toLocalDateTime());
        news.setModificationDate(rs.getTimestamp(6).toLocalDateTime());
        return news;
    }


    @Override
    public Long getNewsRowNum(Long newsId, NewsSearchCriteria criteria) throws DAOException {
        Connection con = DataSourceUtils.getConnection(dataSource);

        String query = generator.newsRowNumber(criteria);

        Long newsRowNum = null;

        try (PreparedStatement ps = con.prepareStatement(query)) {

            ps.setLong(1, newsId);
            try (ResultSet rs = ps.executeQuery()) {

                if (rs.next()) {
                    newsRowNum = rs.getLong(1);
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }

        return newsRowNum;
    }

    @Override
    public Long[] getNextPrevRowNum(Long newsRowNum, NewsSearchCriteria criteria) throws DAOException {

        Connection con = DataSourceUtils.getConnection(dataSource);

        String query = generator.nextPrevRowNum(criteria);


        Long[] nextPrevRowNum = null;

        try (PreparedStatement ps = con.prepareStatement(query)) {

            nextPrevRowNum = new Long[2];

            ps.setLong(1, newsRowNum - 1);
            ps.setLong(2, newsRowNum + 1);

            try (ResultSet rs = ps.executeQuery()) {
                Long rowNumber;
                for (int i = 0; i < 2; i++) {

                    if (rs.next()) {
                        rowNumber = rs.getLong(1);

                        if (rowNumber == newsRowNum + 1) {
                            System.out.println("[1] = " + rowNumber);
                            nextPrevRowNum[1] = rowNumber;
                        } else {
                            if (rowNumber == newsRowNum - 1) {
                                System.out.println("[0] = " + rowNumber);
                                nextPrevRowNum[0] = rowNumber;
                            }
                        }
                    }
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }

        return nextPrevRowNum;
    }

	@Override
	public Long getIdByOrderNum(NewsSearchCriteria searchCriteria, Long orderNum) throws DAOException {
		  Connection con = DataSourceUtils.getConnection(dataSource);

		  
	        String query = generator.newsIdByRowNum(searchCriteria);

	        Long newsId = null;

	        try (PreparedStatement ps = con.prepareStatement(query)) {
	        	
	            ps.setLong(1, orderNum);
	            
	            try (ResultSet rs = ps.executeQuery()) {

	                if (rs.next()) {
	                    newsId = rs.getLong(1);
	                }
	            }
	        } catch (SQLException e) {
	            throw new DAOException(e);
	        } finally {
	            DataSourceUtils.releaseConnection(con, dataSource);
	        }

	        return newsId;
	}


}
