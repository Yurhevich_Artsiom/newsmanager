package com.epam.newsManagerApp.dao.impl;

import com.epam.newsManagerApp.dao.AuthorDAO;
import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.domain.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * {@inheritDoc}
 * <p>
 * Class provides some features set to work with Author
 * entities in database
 */
@Component
public class AuthorDAOImpl implements AuthorDAO {

    private final String SQL_FETCH_ALL = "SELECT AUTHOR_ID, AUTHOR_NAME FROM AUTHORS";
    private final String SQL_CREATE_AUTHOR = "INSERT INTO AUTHORS(AUTHOR_NAME) VALUES(?)";
    private final String SQL_GET_NEWS_AUTH = "SELECT AUTHOR_NAME FROM AUTHORS INNER JOIN NEWS_AUTHORS ON AUTHORS.AUTHOR_ID = NEWS_AUTHORS.AUTHOR_ID WHERE NEWS_AUTHORS.NEWS_ID = ?";
    private final String SQL_FETCH_BY_ID = "SELECT AUTHOR_NAME,EXPIRED FROM AUTHORS WHERE AUTHOR_ID = ?";
    private final String SQL_SET_EXPIRED = "UPDATE AUTHORS SET EXPIRED = ? WHERE AUTHOR_ID = ?";
    private final String SQL_CHECK_AUTHOR = "SELECT AUTHOR_ID FROM AUTHORS WHERE AUTHOR_NAME = ?";
    private final String SQL_UPDATE_AUTHOR = "UPDATE AUTHORS SET AUTHOR_NAME = ? WHERE AUTHOR_ID = ?";
    private static final String SQL_SELECT_AUTHORS = "SELECT * FROM (SELECT a.*, rownum rnum FROM ( SELECT AUTHOR_ID, AUTHOR_NAME FROM AUTHORS WHERE EXPIRED IS NULL) a where rownum <= ? ) where rnum > ?";
    private final static String SQL_COUNT_ALL="SELECT COUNT(*) FROM (SELECT DISTINCT AUTHORS.AUTHOR_ID FROM AUTHORS WHERE EXPIRED IS NULL)";

    @Autowired
    private DataSource dataSource;

    @Override
    public Long insert(Author author) throws DAOException {
        Long id = null;
        Connection con = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement ps = con.prepareStatement(SQL_CREATE_AUTHOR, new String[]{"AUTHOR_ID"})) {

            ps.setString(1, author.getName());
            ps.executeUpdate();

            try (ResultSet rs = ps.getGeneratedKeys()) {
                while (rs.next()) {
                    id = rs.getLong(1);
                }
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }
        return id;
    }

    @Override
    public Author fetch(Long id) throws DAOException {
        Author author = null;
        Connection con = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = con.prepareStatement(SQL_FETCH_BY_ID)) {

            ps.setLong(1, id);
            try (ResultSet rs = ps.executeQuery()) {

                if (rs.next()) {
                    author = new Author();
                    author.setId(id);
                    author.setName(rs.getString(1));
                    author.setExpired(rs.getTimestamp(2) != null ? rs.getTimestamp(2).toLocalDateTime() : null);
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }

        return author;
    }


    @Override
    public Long exist(Author author) throws DAOException {
        Long id = null;
        Connection con = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = con.prepareStatement(SQL_CHECK_AUTHOR)) {

            ps.setString(1, author.getName());
            try (ResultSet rs = ps.executeQuery()) {

                if (rs.next()) {
                    id = rs.getLong(1);
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }
        return id;
    }

    @Override
    public Author getNewsAuthor(Long id) throws DAOException {
        Author author = null;
        Connection con = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = con.prepareStatement(SQL_GET_NEWS_AUTH)) {

            ps.setLong(1, id);
            try (ResultSet rs = ps.executeQuery()) {

                if (rs.next()) {
                    author = new Author();
                    author.setName(rs.getString(1));
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }
        return author;
    }

    @Override
    public List<Author> fetchAll() throws DAOException {

        List<Author> authorList;
        Connection con = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = con.prepareStatement(SQL_FETCH_ALL);
             ResultSet rs = ps.executeQuery();) {

            authorList = new ArrayList<>();

            Author author;

            while (rs.next()) {
                author = new Author();
                author.setId(rs.getLong(1));
                author.setName(rs.getString(2));
                authorList.add(author);
            }


        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }
        return authorList;
    }

    @Override
    public void setExpirationDate(Author author) throws DAOException {
        Connection con = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = con.prepareStatement(SQL_SET_EXPIRED)) {

            ps.setTimestamp(1, Timestamp.valueOf(author.getExpired()));
            ps.setLong(2, author.getId());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }
    }

    @Override
    public List<Author> get(Integer from, Integer count) throws DAOException {

        List<Author> authorList;
        Connection con = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = con.prepareStatement(SQL_SELECT_AUTHORS)) {

            ps.setInt(1, from + count);
            ps.setInt(2, from);

            try (ResultSet rs = ps.executeQuery()) {

                authorList = new ArrayList<>();

                Author author;

                while (rs.next()) {
                    author = new Author();
                    author.setId(rs.getLong(1));
                    author.setName(rs.getString(2));
                    authorList.add(author);
                }
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }
        return authorList;
    }

    @Override
    public int countAll() throws DAOException {
        int count = 0;
        Connection con = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement ps = con.prepareStatement(SQL_COUNT_ALL);
             ResultSet rs = ps.executeQuery()) {

            if (rs.next()) {
                count = rs.getInt(1);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }
        return count; 
        }

    @Override
    public void update(Author author) throws DAOException {
        Connection con = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = con.prepareStatement(SQL_UPDATE_AUTHOR)) {

            ps.setString(1, author.getName());
            ps.setLong(2, author.getId());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }
    }

    @Override
    public void delete(Long id) throws DAOException {

    }

}
