package com.epam.newsManagerApp.dao;

import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.domain.Comment;

import java.util.List;

/**
 * Implementation of this interface providesL
 * set of operations with Comment entities in database
 */
public interface CommentDAO extends GenericDao<Comment> {

    /**
     * Fetches a List of comments from particular
     * news by news' id
     *
     * @param newsId news id
     * @return list of Comment objects
     * @throws DAOException
     */
    List<Comment> getNewsComments(Long newsId) throws DAOException;


    /**
     * Deletes all comments, associated with
     * definite news
     *
     * @param newsId news id
     * @throws DAOException
     */
    void deleteNewsComments(Long newsId) throws DAOException;
}
