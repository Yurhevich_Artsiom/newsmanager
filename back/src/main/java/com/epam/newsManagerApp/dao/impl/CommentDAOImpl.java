package com.epam.newsManagerApp.dao.impl;

import com.epam.newsManagerApp.dao.CommentDAO;
import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.dao.query.QueryGenerator;
import com.epam.newsManagerApp.domain.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * {@inheritDoc}
 * <p>
 * Class provides some features set to work with Comment
 * entities in database
 */

@Component
public class CommentDAOImpl implements CommentDAO {

    private final String SQL_GET_BY_ID = "SELECT NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS WHERE COMMENT_ID = ?";
    private final String SQL_CREATE_COMMENT = "INSERT INTO COMMENTS(NEWS_ID, COMMENT_TEXT,CREATION_DATE) VALUES(?, ?, ?)";
    private final String SQL_DELETE_COMMENT = "DELETE FROM COMMENTS WHERE COMMENT_ID = ?";
    private final String SQL_FETCH_BY_NEWS_ID = "SELECT COMMENT_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS WHERE NEWS_ID = ? ORDER BY CREATION_DATE";
    private final String SQL_DELETE_NEWS_COMMENTS = "DELETE FROM COMMENTS WHERE NEWS_ID = ?";

    @Autowired
    private DataSource dataSource;

    
    @Override
    public Long insert(Comment comment) throws DAOException {
        Long id = null;
        Connection con = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = con.prepareStatement(SQL_CREATE_COMMENT, new String[]{"COMMENT_ID"})) {

            ps.setLong(1, comment.getIdNews());
            ps.setString(2, comment.getComment());
            ps.setTimestamp(3, Timestamp.valueOf(comment.getCreationDate()));
            ps.executeUpdate();

            try (ResultSet rs = ps.getGeneratedKeys()) {

                if (rs.next()) {
                    id = rs.getLong(1);
                }
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }
        return id;
    }

    @Override
    public Comment fetch(Long commentId) throws DAOException {
        Comment comment = null;
        Connection con = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = con.prepareStatement(SQL_GET_BY_ID)) {

            ps.setLong(1, commentId);
            try (ResultSet rs = ps.executeQuery()) {

                if (rs.next()) {
                    comment = new Comment();
                    comment.setId(commentId);
                    comment.setIdNews(rs.getLong(1));
                    comment.setComment(rs.getString(2));
                    comment.setCreationDate(rs.getTimestamp(3).toLocalDateTime());
                }
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }
        return comment;
    }

    @Override
    public void delete(Long commentId) throws DAOException {
        Connection con = DataSourceUtils.getConnection(dataSource);

        try (PreparedStatement ps = con.prepareStatement(SQL_DELETE_COMMENT)) {

            ps.setLong(1, commentId);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }
    }


    @Override
    public List<Comment> getNewsComments(Long newsId) throws DAOException {
        ArrayList<Comment> comments;
        Connection con = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = con.prepareStatement(SQL_FETCH_BY_NEWS_ID)) {

            ps.setLong(1, newsId);

            try (ResultSet rs = ps.executeQuery()) {

                comments = new ArrayList<>();

                while (rs.next()) {
                    Comment comment = new Comment();
                    comment.setId(rs.getLong(1));
                    comment.setComment(rs.getString(2));
                    comment.setCreationDate(rs.getTimestamp(3).toLocalDateTime());

                    comments.add(comment);
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }

        return comments;
    }

    @Override
    public void deleteNewsComments(Long newsId) throws DAOException {
        Connection con = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = con.prepareStatement(SQL_DELETE_NEWS_COMMENTS)) {

            ps.setLong(1, newsId);

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DataSourceUtils.releaseConnection(con, dataSource);
        }
    }

    @Override
    public void update(Comment instance) throws DAOException {

    }

}
