package com.epam.newsManagerApp.dao;

import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.domain.Tag;

import java.util.List;

/**
 * Implementation of this interface allows to process
 * set of  operations with Tag entities in database
 */
public interface TagDAO extends GenericDao<Tag> {

    /**
     * Fetches a List of tags from definite news by news' id
     *
     * @param newsId news id
     * @return list of Tag objects
     * @throws DAOException
     */
    List<Tag> fetchByNewsId(Long newsId) throws DAOException;


    /**
     * Checks if tag already exists
     * in database
     *
     * @param tag instance to check
     * @return id of tag if exists, null if not
     * @throws DAOException
     */
    Long exist(Tag tag) throws DAOException;

    /**
     * Fetches all Tag entities from
     * database
     *
     * @return list of Tag objects
     * @throws DAOException
     */
    List<Tag> getAll() throws DAOException;

    /**
     * Unlink definite tag from all associations
     * with news
     *
     * @param tagId tag's id
     * @throws DAOException
     */
    void unlinkTagFromAllNews(Long tagId) throws DAOException;

    int countAll() throws DAOException;


	List<Tag> get(Integer from, Integer count) throws DAOException;
    
    
}

