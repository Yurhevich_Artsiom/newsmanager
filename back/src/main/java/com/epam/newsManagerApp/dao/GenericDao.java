package com.epam.newsManagerApp.dao;

import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.domain.Entity;

/**
 * Interface provides basic CRUD operations
 */
public interface GenericDao<T extends Entity> {

    /**
     * Inserts a new record of instance into database
     *
     * @param instance domain to insert
     * @return id of inserted domain
     * @throws DAOException
     */
    Long insert(T instance) throws DAOException;

    /**
     * Fetches an instance row by id from database
     *
     * @param id domain id
     * @return fetched domain
     * @throws DAOException
     */
    T fetch(Long id) throws DAOException;

    /**
     * Updates an instance row in database
     *
     * @param instance domain to update
     * @throws DAOException
     */
    void update(T instance) throws DAOException;

    /**
     * Deletes an instance row from database
     *
     * @param id domain's id to delete
     * @throws DAOException
     */
    void delete(Long id) throws DAOException;

}
