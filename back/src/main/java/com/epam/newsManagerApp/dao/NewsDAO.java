package com.epam.newsManagerApp.dao;

import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.domain.News;
import com.epam.newsManagerApp.transfer.search.NewsSearchCriteria;

import java.util.List;

/**
 * Implementation of this interface providesL
 * set of operations with News entities in database
 */
public interface NewsDAO extends GenericDao<News> {

    /**
     * Fetch a list of news. Searching executes according to
     * NewsSearchCriteria object by expected
     * tags, author and sorting by num of comments
     *
     * @param searchCriteria NewsSearchCriteria object to find
     *                       news with expected parameters
     * @return a list of news
     * @throws DAOException
     */

    List<News> getNews(NewsSearchCriteria searchCriteria) throws DAOException;

    /**
     * Associates list of tags with definite news
     *
     * @param newsId news id
     * @param tagsId list of tag's id
     * @throws DAOException
     */

    void linkToTags(Long newsId, List<Long> tagsId) throws DAOException;


    /**
     * Unlink the associated author from the
     * definite news
     *
     * @param newsId news' id
     * @throws DAOException
     */
    void unlinkAuthor(Long newsId) throws DAOException;

    /**
     * Unlink the associated tags from the
     * definite news
     *
     * @param newsId news' id
     * @throws DAOException
     */

    void unlinkTags(Long newsId) throws DAOException;

    /**
     * Counts all News entities
     * in database
     *
     * @return count of news
     * @throws DAOException
     */

    int countNews() throws DAOException;

    /**
     * Counts all News entities
     * in database
     *
     * @return count of news
     * @throws DAOException
     */

    int countNews(NewsSearchCriteria searchCriteria) throws DAOException;

    /**
     * Associates an author with definite news
     *
     * @param newsId news id
     * @param autId  author id
     * @throws DAOException
     */

    void linkToAuthor(Long newsId, Long autId) throws DAOException;

	Long getNewsRowNum(Long newsId, NewsSearchCriteria criteria) throws DAOException;

    Long[] getNextPrevRowNum(Long newsId, NewsSearchCriteria criteria) throws DAOException;

	Long getIdByOrderNum(NewsSearchCriteria searchCriteria, Long orderNum) throws DAOException;
	

}

