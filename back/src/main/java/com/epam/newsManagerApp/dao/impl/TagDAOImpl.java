package com.epam.newsManagerApp.dao.impl;

import com.epam.newsManagerApp.dao.TagDAO;
import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.domain.Author;
import com.epam.newsManagerApp.domain.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * {@inheritDoc}
 * <p>
 * Class provides some features set to work with Tag entities in database
 */

@Component
public class TagDAOImpl implements TagDAO {

	private final String SQL_FETCH_BY_TAG_ID = "SELECT TAGS.TAG_ID, TAGS.TAG_NAME FROM TAGS WHERE TAG_ID=?";
	private final String SQL_INSERT_TAG = "INSERT INTO TAGS(TAG_NAME) VALUES(?)";
	private final String SQL_FETCH_BY_NEWS_ID = "SELECT TAGS.TAG_ID, TAGS.TAG_NAME FROM TAGS INNER JOIN NEWS_TAGS ON NEWS_TAGS.TAG_ID = TAGS.TAG_ID WHERE NEWS_TAGS.NEWS_ID = ?";
	private final String SQL_GET_BY_NAME = "SELECT TAG_ID FROM TAGS WHERE TAG_NAME = ?";
	private final String SQL_FETCH_ALL = "SELECT TAGS.TAG_ID, TAGS.TAG_NAME FROM TAGS";
	private final String SQL_UNLINK_FROM_ALL_NEWS = "DELETE FROM NEWS_TAGS WHERE NEWS_TAGS.TAG_ID = ?";
	private final String SQL_UPDATE_TAG = "UPDATE TAGS SET TAGS.TAG_NAME = ? WHERE TAGS.TAG_ID = ?";
	private final String SQL_DELETE_TAG = "DELETE FROM TAGS WHERE TAGS.TAG_ID = ?";

	private static final String SQL_SELECT_TAGS = "SELECT * FROM (SELECT a.*, rownum rnum FROM ( SELECT TAG_ID, TAG_NAME FROM TAGS) a where rownum <= ? ) where rnum > ?";
	private final static String SQL_COUNT_ALL = "SELECT COUNT(*) FROM (SELECT DISTINCT TAGS.TAG_ID FROM TAGS)";

	@Autowired
	private DataSource dataSource;

	@Override
	public Long insert(Tag tag) throws DAOException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		Long id = null;

		try (PreparedStatement ps = con.prepareStatement(SQL_INSERT_TAG, new String[] { "TAG_ID" })) {

			ps.setString(1, tag.getTag());
			ps.executeUpdate();

			try (ResultSet rs = ps.getGeneratedKeys()) {

				if (rs.next()) {
					id = rs.getLong(1);
				}
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}

		return id;
	}

	@Override
	public List<Tag> fetchByNewsId(Long newsId) throws DAOException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		List<Tag> tags;

		try (PreparedStatement ps = con.prepareStatement(SQL_FETCH_BY_NEWS_ID)) {

			ps.setLong(1, newsId);

			try (ResultSet rs = ps.executeQuery()) {
				tags = new ArrayList<>();

				while (rs.next()) {
					Tag tag = new Tag();
					tag.setId(rs.getLong(1));
					tag.setTag(rs.getString(2));
					tags.add(tag);
				}
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return tags;

	}

	@Override
	public Long exist(Tag tag) throws DAOException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		Long id = null;

		try (PreparedStatement ps = con.prepareStatement(SQL_GET_BY_NAME)) {

			ps.setString(1, tag.getTag());

			try (ResultSet rs = ps.executeQuery()) {

				if (rs.next()) {
					id = rs.getLong(1);
				}
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return id;
	}

	@Override
	public List<Tag> getAll() throws DAOException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		List<Tag> tags;

		try (PreparedStatement ps = con.prepareStatement(SQL_FETCH_ALL); ResultSet rs = ps.executeQuery()) {

			tags = new ArrayList<>();

			while (rs.next()) {
				Tag tag = new Tag();
				tag.setId(rs.getLong(1));
				tag.setTag(rs.getString(2));
				tags.add(tag);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return tags;
	}

	@Override
	public void unlinkTagFromAllNews(Long tagId) throws DAOException {

		Connection con = DataSourceUtils.getConnection(dataSource);

		try (PreparedStatement ps = con.prepareStatement(SQL_UNLINK_FROM_ALL_NEWS)) {

			ps.setLong(1, tagId);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public void update(Tag tag) throws DAOException {

		Connection con = DataSourceUtils.getConnection(dataSource);

		try (PreparedStatement ps = con.prepareStatement(SQL_UPDATE_TAG)) {

			ps.setString(1, tag.getTag());
			ps.setLong(2, tag.getId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public void delete(Long id) throws DAOException {

		Connection con = DataSourceUtils.getConnection(dataSource);

		try (PreparedStatement ps = con.prepareStatement(SQL_DELETE_TAG)) {

			ps.setLong(1, id);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}

	@Override
	public Tag fetch(Long tagId) throws DAOException {

		Connection con = DataSourceUtils.getConnection(dataSource);
		Tag tag = null;

		try (PreparedStatement ps = con.prepareStatement(SQL_FETCH_BY_TAG_ID)) {

			ps.setLong(1, tagId);

			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					tag = new Tag();
					tag.setId(rs.getLong(1));
					tag.setTag(rs.getString(2));
				}
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return tag;
	}

	@Override
	public int countAll() throws DAOException {
		int count = 0;
		Connection con = DataSourceUtils.getConnection(dataSource);

		try (PreparedStatement ps = con.prepareStatement(SQL_COUNT_ALL); ResultSet rs = ps.executeQuery()) {

			if (rs.next()) {
				count = rs.getInt(1);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return count;
	}

	@Override
	public List<Tag> get(Integer from, Integer count) throws DAOException {
		List<Tag> tagList;
		Connection con = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement ps = con.prepareStatement(SQL_SELECT_TAGS)) {

			ps.setInt(1, from + count);
			ps.setInt(2, from);

			try (ResultSet rs = ps.executeQuery()) {

				tagList = new ArrayList<>();

				Tag tag;

				while (rs.next()) {
					tag = new Tag();
					tag.setId(rs.getLong(1));
					tag.setTag(rs.getString(2));
					tagList.add(tag);
				}
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		return tagList;
	}

}
