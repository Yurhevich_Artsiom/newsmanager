package com.epam.newsManagerApp.dao;

import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.domain.Author;
import com.epam.newsManagerApp.transfer.search.AuthorSearchCriteria;

import java.util.List;

/**
 * Implementation of this interface provides
 * set of operations with Authors entities in database
 */
public interface AuthorDAO extends GenericDao<Author> {

    /**
     * Checks if author already exists
     * in database
     *
     * @param author instance to check
     * @return id of author if exists, null if not
     * @throws DAOException
     */
    Long exist(Author author) throws DAOException;

    /**
     * Fetches the author of the news by
     * it's id
     *
     * @param id news id
     * @return fetched author
     * @throws DAOException
     */
    Author getNewsAuthor(Long id) throws DAOException;


    /**
     * Fetches all author entities from
     * database
     *
     * @return List of Author object
     * @throws DAOException
     */
    List<Author> fetchAll() throws DAOException;

    /**
     * Sets the expiration date to the
     * given author
     *
     * @param author author to set expired
     * @throws DAOException
     */
    void setExpirationDate(Author author) throws DAOException;


    List<Author> get(Integer from, Integer count) throws DAOException;

    int countAll() throws DAOException;
}
