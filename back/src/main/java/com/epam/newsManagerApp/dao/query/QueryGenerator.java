package com.epam.newsManagerApp.dao.query;

import com.epam.newsManagerApp.transfer.search.NewsSearchCriteria;

import java.util.List;

import org.springframework.stereotype.Component;

/**
 * Class provides generating database queries according to the giving parameters
 */

@Component
public class QueryGenerator {

	private static final String SELECT_NEWS = "SELECT DISTINCT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE";
	private static final String SELECT_COUNT = " SELECT COUNT(*) FROM (";
	private static final String SELECT_NEWS_ID = "SELECT DISTINCT NEWS.NEWS_ID";
	private static final String SELECT_ID_AND_ROWNUM = "SELECT NEWS_ID , ROWNUM rnum FROM ( ";
	private static final String SELECT_RNUM = "SELECT RNUM FROM (";
	private static final String SELECT_ID = "SELECT NEWS_ID FROM(";
	private static final String JOIN_NEWS_TAGS = " INNER JOIN NEWS_TAGS ON NEWS.NEWS_ID = NEWS_TAGS.NEWS_ID";
	private static final String JOIN_AUTHORS = " INNER JOIN NEWS_AUTHORS ON NEWS.NEWS_ID = NEWS_AUTHORS.NEWS_ID INNER JOIN AUTHORS ON NEWS_AUTHORS.AUTHOR_ID = AUTHORS.AUTHOR_ID";
	private static final String JOIN_COMMENTS = " LEFT JOIN COMMENTS ON COMMENTS.NEWS_ID = NEWS.NEWS_ID";
	private static final String NUM_COMMENTS = ", COUNT(COMMENTS.NEWS_ID) AS num_comments";
	private static final String FROM_NEWS = " FROM NEWS ";
	private static final String WHERE = " WHERE ";
	private static final String AND = " AND ";
	private static final String AUTHOR_NAME_IN = "AUTHORS.AUTHOR_NAME IN(";
	private static final String GROUP_BY_FULL = " GROUP BY COMMENTS.NEWS_ID, NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE ";
	private static final String ORDER_BY_NUM_COM_AND_DATE = "ORDER BY num_comments DESC, NEWS.CREATION_DATE DESC";
	private static final String ORDER_BY_NUM_COM="ORDER BY num_comments DESC";
	private static final String SELECT_LIMIT = "select * from (select a.*, rownum rnum from (";
	private static final String MAX_ROW = ") a where rownum <= ";
	private static final String MIN_ROW = ") where rnum > ";
	private static final String TAG_MATCH = "EXISTS (SELECT 1 FROM NEWS_TAGS INNER JOIN TAGS ON NEWS_TAGS.TAG_ID=TAGS.TAG_ID WHERE NEWS_ID=NEWS.NEWS_ID AND TAGS.TAG_NAME='";
	private static final String NEWS_ID_EQ = "NEWS_ID=?";
	private static final Object OR = " OR ";
	private static final Object ROW_NUM_EQ = "RNUM=?";
	private static final Object GROUP_BY_ID = " GROUP BY NEWS.NEWS_ID ";


	/**
	 * Constructs query String to search news according to the
	 * NewsSearchCriteria object, which may include such expected parameters as
	 * tags, author and boolean value, that represents sorting news by their
	 * number of comments
	 *
	 * @param searchCriteria
	 *            NewsSearchCriteria object to find news with expected
	 *            parameters
	 * @return Constructed query String
	 */
	public String generateSelectNewsQuery(NewsSearchCriteria searchCriteria) {

		StringBuilder queryBuilder = new StringBuilder();

		List<String> tags = searchCriteria.getTags();
		List<String> authors = searchCriteria.getAuthors();

		boolean sortedByCmts = searchCriteria.isSortedByNumOfCom();
		boolean fetchByTag = tags != null && !tags.isEmpty();
		boolean fetchByAuth = authors != null && !authors.isEmpty();
		boolean rowLimit = searchCriteria.getFrom() != null && searchCriteria.getCount() != null;

		if (rowLimit) {
			queryBuilder.append(SELECT_LIMIT);
		}

		queryBuilder.append(SELECT_NEWS);

		if (sortedByCmts) {
			queryBuilder.append(NUM_COMMENTS);
		}

		queryBuilder.append(FROM_NEWS);

		if (fetchByTag) {
			queryBuilder.append(JOIN_NEWS_TAGS);
		}

		if (fetchByAuth) {
			queryBuilder.append(JOIN_AUTHORS);
		}

		if (sortedByCmts) {
			queryBuilder.append(JOIN_COMMENTS);
		}

		if (fetchByTag || fetchByAuth) {
			queryBuilder.append(WHERE);
		}

		if (fetchByTag) {

			addTagCondition(tags, queryBuilder);

		}

		if (fetchByAuth) {
			if (fetchByTag) {
				queryBuilder.append(AND);
			}

			addAuthorCondition(authors, queryBuilder);
		}

		if (sortedByCmts) {

			queryBuilder.append(GROUP_BY_FULL);

			queryBuilder.append(ORDER_BY_NUM_COM_AND_DATE);

		}

		if (rowLimit) {
			queryBuilder.append(MAX_ROW);
			queryBuilder.append(searchCriteria.getFrom() + searchCriteria.getCount());
			queryBuilder.append(MIN_ROW);
			queryBuilder.append(searchCriteria.getFrom());
		}

		return queryBuilder.toString();

	}

	public String generateCountNewsQuery(NewsSearchCriteria searchCriteria) {

		StringBuilder queryBuilder = new StringBuilder(SELECT_COUNT);

		queryBuilder.append(SELECT_NEWS_ID);

		List<String> tags = searchCriteria.getTags();
		List<String> authors = searchCriteria.getAuthors();

		boolean fetchByTag = tags != null && !tags.isEmpty();
		boolean fetchByAuth = authors != null && !authors.isEmpty();

		queryBuilder.append(FROM_NEWS);

		if (fetchByTag) {
			queryBuilder.append(JOIN_NEWS_TAGS);
		}

		if (fetchByAuth) {
			queryBuilder.append(JOIN_AUTHORS);
		}

		if (fetchByTag || fetchByAuth) {
			queryBuilder.append(WHERE);
		}

		if (fetchByTag) {
			addTagCondition(tags, queryBuilder);

		}

		if (fetchByAuth) {
			if (fetchByTag) {
				queryBuilder.append(AND);
			}

			addAuthorCondition(authors, queryBuilder);

		}

		queryBuilder.append(')');

		return queryBuilder.toString();

	}

	public String newsRowNumber(NewsSearchCriteria searchCriteria) {

		List<String> tags = searchCriteria.getTags();
		List<String> authors = searchCriteria.getAuthors();

		boolean fetchByTag = tags != null && !tags.isEmpty();
		boolean fetchByAuth = authors != null && !authors.isEmpty();
		boolean sortedByCmts = searchCriteria.isSortedByNumOfCom();

		StringBuilder queryBuilder = new StringBuilder(SELECT_RNUM);

		queryBuilder.append(SELECT_ID_AND_ROWNUM);

		queryBuilder.append(SELECT_NEWS_ID);

		if (sortedByCmts) {
			queryBuilder.append(NUM_COMMENTS);
		}

		queryBuilder.append(FROM_NEWS);

		if (fetchByTag) {
			queryBuilder.append(JOIN_NEWS_TAGS);
		}

		if (fetchByAuth) {
			queryBuilder.append(JOIN_AUTHORS);
		}

		if (sortedByCmts) {
			queryBuilder.append(JOIN_COMMENTS);
		}

		if (fetchByTag || fetchByAuth) {
			queryBuilder.append(WHERE);
		}

		if (fetchByTag) {

			addTagCondition(tags, queryBuilder);

		}

		if (fetchByAuth) {
			if (fetchByTag) {
				queryBuilder.append(AND);
			}
			addAuthorCondition(authors, queryBuilder);
		}

		if (sortedByCmts) {

			queryBuilder.append(GROUP_BY_ID);

			queryBuilder.append(ORDER_BY_NUM_COM);

		}

		queryBuilder.append(')');

		queryBuilder.append(')');

		queryBuilder.append(WHERE);

		queryBuilder.append(NEWS_ID_EQ);

		return queryBuilder.toString();
	}

	public String nextPrevRowNum(NewsSearchCriteria searchCriteria) {

		List<String> tags = searchCriteria.getTags();
		List<String> authors = searchCriteria.getAuthors();

		boolean fetchByTag = tags != null && !tags.isEmpty();
		boolean fetchByAuth = authors != null && !authors.isEmpty();
		boolean sortedByCmts = searchCriteria.isSortedByNumOfCom();

		StringBuilder queryBuilder = new StringBuilder(SELECT_RNUM);

		queryBuilder.append(SELECT_ID_AND_ROWNUM);

		queryBuilder.append(SELECT_NEWS_ID);

		if (sortedByCmts) {
			queryBuilder.append(NUM_COMMENTS);
		}

		queryBuilder.append(FROM_NEWS);

		if (fetchByTag) {
			queryBuilder.append(JOIN_NEWS_TAGS);
		}

		if (fetchByAuth) {
			queryBuilder.append(JOIN_AUTHORS);
		}

		if (sortedByCmts) {
			queryBuilder.append(JOIN_COMMENTS);
		}

		if (fetchByTag || fetchByAuth) {
			queryBuilder.append(WHERE);
		}

		if (fetchByTag) {
			addTagCondition(tags, queryBuilder);

		}

		if (fetchByAuth) {
			if (fetchByTag) {
				queryBuilder.append(AND);
			}
			addAuthorCondition(authors, queryBuilder);
		}

		if (sortedByCmts) {

			queryBuilder.append(GROUP_BY_ID);

			queryBuilder.append(ORDER_BY_NUM_COM);

		}

		queryBuilder.append(')');

		queryBuilder.append(')');

		queryBuilder.append(WHERE);

		queryBuilder.append(ROW_NUM_EQ);

		queryBuilder.append(OR);

		queryBuilder.append(ROW_NUM_EQ);

		return queryBuilder.toString();
	}

	private void addAuthorCondition(List<String> authors, StringBuilder queryBuilder) {
		queryBuilder.append(AUTHOR_NAME_IN);
		for (int i = 0; i < authors.size(); i++) {

			queryBuilder.append("'").append(authors.get(i)).append("'");

			if (i != authors.size() - 1) {
				queryBuilder.append(',');
			}

		}
		queryBuilder.append(')');
	}

	private void addTagCondition(List<String> tags, StringBuilder queryBuilder) {

		for (int i = 0; i < tags.size(); i++) {

			queryBuilder.append(TAG_MATCH);

			queryBuilder.append(tags.get(i)).append("')");

			if (i != tags.size() - 1) {
				queryBuilder.append(AND);
			}

		}
	}

	public String newsIdByRowNum(NewsSearchCriteria searchCriteria) {
		List<String> tags = searchCriteria.getTags();
		List<String> authors = searchCriteria.getAuthors();

		boolean fetchByTag = tags != null && !tags.isEmpty();
		boolean fetchByAuth = authors != null && !authors.isEmpty();
		boolean sortedByCmts = searchCriteria.isSortedByNumOfCom();

		StringBuilder queryBuilder = new StringBuilder(SELECT_ID);

		queryBuilder.append(SELECT_ID_AND_ROWNUM);

		queryBuilder.append(SELECT_NEWS_ID);

		if (sortedByCmts) {
			queryBuilder.append(NUM_COMMENTS);
		}

		queryBuilder.append(FROM_NEWS);

		if (fetchByTag) {
			queryBuilder.append(JOIN_NEWS_TAGS);
		}

		if (fetchByAuth) {
			queryBuilder.append(JOIN_AUTHORS);
		}

		if (sortedByCmts) {
			queryBuilder.append(JOIN_COMMENTS);
		}

		if (fetchByTag || fetchByAuth) {
			queryBuilder.append(WHERE);
		}

		if (fetchByTag) {
			addTagCondition(tags, queryBuilder);

		}

		if (fetchByAuth) {
			if (fetchByTag) {
				queryBuilder.append(AND);
			}
			addAuthorCondition(authors, queryBuilder);
		}

		if (sortedByCmts) {

			queryBuilder.append(GROUP_BY_ID);

			queryBuilder.append(ORDER_BY_NUM_COM);

		}

		queryBuilder.append(')');

		queryBuilder.append(')');

		queryBuilder.append(WHERE);

		queryBuilder.append(ROW_NUM_EQ);

		return queryBuilder.toString();
	}
}
