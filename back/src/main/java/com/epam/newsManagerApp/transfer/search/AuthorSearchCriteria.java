package com.epam.newsManagerApp.transfer.search;

import com.epam.newsManagerApp.domain.Author;

import java.util.List;

/**
 * Created by Artsiom_Yurhevich on 8/3/2016.
 */
public class AuthorSearchCriteria {

    private Integer from;
    private Integer count;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }
}
