package com.epam.newsManagerApp.transfer;

import com.epam.newsManagerApp.domain.Author;
import com.epam.newsManagerApp.domain.Comment;
import com.epam.newsManagerApp.domain.News;
import com.epam.newsManagerApp.domain.Tag;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing the representation of association
 * between news, tags, comments and author
 */

public class NewsTransferObject {
    private News news;
    private List<Tag> tags;
    private List<Comment> comments;
    private Author author;

    public NewsTransferObject() {
        super();
        news = new News();
        tags = new ArrayList<>();
        comments = new ArrayList<>();
        author = new Author();
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((author == null) ? 0 : author.hashCode());
        result = prime * result + ((comments == null) ? 0 : comments.hashCode());
        result = prime * result + ((news == null) ? 0 : news.hashCode());
        result = prime * result + ((tags == null) ? 0 : tags.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        NewsTransferObject other = (NewsTransferObject) obj;

        if (author == null) {
            if (other.author != null) {
                return false;
            }
        } else {
            if (!author.equals(other.author)) {
                return false;
            }
        }
        if (comments == null) {
            if (other.comments != null) {
                return false;
            }
        } else {
            if (!comments.equals(other.comments)) {
                return false;
            }
        }
        if (news == null) {
            if (other.news != null) {
                return false;
            }
        } else {
            if (!news.equals(other.news)) {
                return false;
            }
        }
        if (tags == null) {
            if (other.tags != null) {
                return false;
            }
        } else {
            if (!tags.equals(other.tags)) {
                return false;
            }
        }
        return true;
    }



}
