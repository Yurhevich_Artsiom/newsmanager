package com.epam.newsManagerApp.transfer.search;


import java.util.List;

/**
 * This class is used for searching appropriate
 * news transfer objects by expected tags, author
 * and sorting by number of comments
 */

public class NewsSearchCriteria {

    private List<String> authors;
    private List<String> tags;
    private boolean sortedByNumOfCom = true;
    private Integer from;
    private Integer count;


   

  

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public boolean isSortedByNumOfCom() {
        return sortedByNumOfCom;
    }

    public void setSortedByNumOfCom(boolean sortedByNumOfCom) {
        this.sortedByNumOfCom = sortedByNumOfCom;
    }

	public Integer getFrom() {
		return from;
	}

	public void setFrom(Integer from) {
		this.from = from;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
    
    
}
