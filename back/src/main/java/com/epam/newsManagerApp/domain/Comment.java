package com.epam.newsManagerApp.domain;

import java.time.LocalDateTime;


public class Comment extends Entity {

    Long idNews;

    private String comment;

    private LocalDateTime creationDate;


    public Comment() {
    }

    public Comment(Long id, Long idNews, String comment, LocalDateTime creationTime) {
        super(id);
        this.idNews = idNews;
        this.comment = comment;
        this.creationDate = creationTime;
    }

    public Long getIdNews() {
        return idNews;
    }

    public void setIdNews(Long idNews) {
        this.idNews = idNews;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationTime) {
        this.creationDate = creationTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Comment comment1 = (Comment) o;

        if (idNews != comment1.idNews) {
            return false;
        }
        if (comment != null ? !comment.equals(comment1.comment) : comment1.comment != null) {
            return false;
        }
        return creationDate != null ? creationDate.equals(comment1.creationDate) : comment1.creationDate == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (idNews != null ? idNews.hashCode() : 0);
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        return result;
    }


}
