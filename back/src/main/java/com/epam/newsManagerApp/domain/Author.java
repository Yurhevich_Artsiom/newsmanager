package com.epam.newsManagerApp.domain;

import java.time.LocalDateTime;


public class Author extends Entity {

    private String name;

    private LocalDateTime expired;

    public Author() {
    }

    public Author(Long id, String name, LocalDateTime expired) {
        super(id);
        this.name = name;
        this.expired = expired;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getExpired() {
        return expired;
    }

    public void setExpired(LocalDateTime expired) {
        this.expired = expired;
    }


    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Author author = (Author) o;

        if (name != null ? !name.equals(author.name) : author.name != null) {
            return false;
        }
        return expired != null ? expired.equals(author.expired) : author.expired == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (expired != null ? expired.hashCode() : 0);
        return result;
    }


    @Override
    public String toString() {
        return "Author{" +
                "name='" + name + '\'' +
                ", expired=" + expired +
                '}';
    }
}
