package com.epam.newsManagerApp.domain;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

public class News extends Entity {


    private String title;

    private String shortText;

    private String fullText;


    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime creationDate;

    private LocalDateTime modificationDate;

    public News() {
    }

    public News(Long id, String title, String shortText, String fullText, LocalDateTime creationTime, LocalDateTime modificationDate) {
        super(id);
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.creationDate = creationTime;
        this.modificationDate = modificationDate;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(LocalDateTime modificationDate) {
        this.modificationDate = modificationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        News news = (News) o;

        if (title != null ? !title.equals(news.title) : news.title != null) {
            return false;
        }
        if (shortText != null ? !shortText.equals(news.shortText) : news.shortText != null) {
            return false;
        }
        if (fullText != null ? !fullText.equals(news.fullText) : news.fullText != null) {
            return false;
        }
        if (creationDate != null ? !creationDate.equals(news.creationDate) : news.creationDate != null) {
            return false;
        }
        return modificationDate != null ? modificationDate.isEqual(news.modificationDate) : news.modificationDate == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (shortText != null ? shortText.hashCode() : 0);
        result = 31 * result + (fullText != null ? fullText.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (modificationDate != null ? modificationDate.hashCode() : 0);
        return result;
    }


    @Override
    public String toString() {
        return "News{" +
                "title='" + title + '\'' +
                ", shortText='" + shortText + '\'' +
                ", fullText='" + fullText + '\'' +
                ", creationDate=" + creationDate +
                ", modificationDate=" + modificationDate +
                '}';
    }
}
