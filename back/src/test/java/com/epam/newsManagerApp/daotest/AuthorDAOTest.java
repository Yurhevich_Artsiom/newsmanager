package com.epam.newsManagerApp.daotest;

import com.epam.newsManagerApp.dao.AuthorDAO;
import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.domain.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Artsiom_Yurhevich on 6/22/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-context.xml")
@DatabaseSetup("classpath:/testdata/AuthorsData.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
@DatabaseTearDown(value = "classpath:/testdata/AuthorsData.xml", type = DatabaseOperation.DELETE_ALL)
//@ActiveProfiles("test")

public class AuthorDAOTest {

    @Autowired
    private AuthorDAO authorDAO;

    private Author author;



    @Test
    public void testInsert() {
        try {
            author = new Author(null, "TEST_AUTH", null);
            assertTrue(authorDAO.insert(author) > 0);
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }

    @Test(expected = DAOException.class)
    public void testInsertNullName() throws DAOException {
        author = new Author(null, null, null);
        authorDAO.insert(author);
    }


    @Test
    public void setExpiredTest() {
        try {
            author = new Author(null, "TEST_AUTH", null);
            Long id = authorDAO.insert(author);
            author.setId(id);
            author.setExpired(LocalDateTime.now());
            authorDAO.setExpirationDate(author);
            assertEquals(author, authorDAO.fetch(id));

        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }

    @Test(expected = NullPointerException.class)
    public void setExpiredTestForNull() {
        try {
            author = new Author(null, "TEST_AUTH", null);
            authorDAO.setExpirationDate(author);
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }

    @Test
    public void testFetchAll() {
        try {
            List<Author> authorList = authorDAO.fetchAll();
            assertTrue(!authorList.isEmpty());
        } catch (DAOException e) {
            fail("DAO execution fail");
        }
    }

    @Test
    public void testFetchByNewsId() {
        try {
            assertNotNull(authorDAO.getNewsAuthor(1L));
        } catch (DAOException e) {
            fail("DAO execution fail");
        }
    }

    @Test(expected = NullPointerException.class)
    public void testFetchByNullNewsId() throws DAOException {
        try {
            authorDAO.getNewsAuthor(null);
        } catch (DAOException e) {
            fail("DAO execution fail");
        }
    }

    @Test
    public void testExist() {
        try {
            author = new Author(null, "TEST_AUTH", null);
            authorDAO.insert(author);
            assertTrue(authorDAO.exist(author) > 0);
        } catch (DAOException e) {
            fail("DAO execution failed");
        }

    }

    @Test
    public void testUpdate()
    {
        try {
            author = new Author(null, "TEST_AUTH", null);
            Long id = authorDAO.insert(author);
            author.setId(id);
            author.setName("NEW_TEST_NAME");
            authorDAO.update(author);

            assertEquals(author.getName(), authorDAO.fetch(id).getName());

        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }
}