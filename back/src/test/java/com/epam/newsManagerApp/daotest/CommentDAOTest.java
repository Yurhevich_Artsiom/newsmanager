package com.epam.newsManagerApp.daotest;

import com.epam.newsManagerApp.dao.CommentDAO;
import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.domain.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Artsiom_Yurhevich on 24.06.2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-context.xml")
@DatabaseSetup("classpath:/testdata/CommentsData.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
@DatabaseTearDown(value = "classpath:/testdata/CommentsData.xml", type = DatabaseOperation.DELETE_ALL)

public class CommentDAOTest {

    @Autowired
    private CommentDAO commentDAO;

    Comment comment;


    @Test
    public void testInsert() {
        comment = new Comment(null, 1L, "test comment", LocalDateTime.now());
        try {
            assertTrue(commentDAO.insert(comment) > 0);
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }

    @Test(expected = DAOException.class)
    public void testInsertNullComment() throws DAOException {

        comment = new Comment(null, 1L, null, LocalDateTime.now());
        commentDAO.insert(comment);

    }

    @Test
    public void testRead() {
        comment = new Comment(null, 1L, "test comment", LocalDateTime.now());
        try {
            Long id = commentDAO.insert(comment);
            comment.setId(id);
            assertEquals(comment, commentDAO.fetch(id));
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }

    @Test
    public void testDeleteComment() {
        comment = new Comment(null, 1L, "test comment", LocalDateTime.now());
        try {
            Long id = commentDAO.insert(comment);
            comment.setId(id);
            commentDAO.delete(id);
            assertNull(commentDAO.fetch(id));

        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }

    @Test(expected = NullPointerException.class)
    public void testDeleteNullComment() throws DAOException {
        commentDAO.delete(null);
    }


    @Test
    public void testGetNewsComments() {
        try {
            List<Comment> comments = commentDAO.getNewsComments(2L);
            assertFalse(comments.isEmpty());
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }


    @Test
    public void testDeleteNewsComments(){
        comment = new Comment(null, 1L, "test comment", LocalDateTime.now());
        try {
            commentDAO.insert(comment);
            commentDAO.deleteNewsComments(1L);
            assertTrue(commentDAO.getNewsComments(1L).isEmpty());

        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }


}
