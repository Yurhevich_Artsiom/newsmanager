package com.epam.newsManagerApp.daotest;

import com.epam.newsManagerApp.dao.TagDAO;
import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.domain.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Artsiom_Yurhevich on 24.06.2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-context.xml")
@DatabaseSetup("classpath:/testdata/TagsData.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
@DatabaseTearDown(value = "classpath:/testdata/TagsData.xml", type = DatabaseOperation.DELETE_ALL)

public class TagDAOTest {


    @Autowired
    private TagDAO tagDAO;

    private Tag tag;

    private List<Tag> tags;


    @Test
    public void testInsertTag() {
        try {
            tag = new Tag(null, "Test tag");
            assertTrue(tagDAO.insert(tag) > 0);
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }


    @Test(expected = DAOException.class)
    public void testInsertNullTag() throws DAOException {
            tag = new Tag(null, null);
            tagDAO.insert(tag);
    }

    @Test
    public void testFetch() {
        tag = new Tag(null, "Test tag");
        try {
            Long id = tagDAO.insert(tag);
            assertEquals(tag.getTag(), tagDAO.fetch(id).getTag());
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }

    @Test(expected = NullPointerException.class)
    public void testFetchNull() {
        try {
            tagDAO.fetch(null);
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }

    @Test
    public void testExist() {
        tag = new Tag(null, "Test tag");
        try {
            tagDAO.insert(tag);
            assertTrue(tagDAO.exist(tag) > 0);
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }

    @Test
    public void testExistNullTag() throws DAOException {
        tag = new Tag(null, null);
        assertTrue(tagDAO.exist(tag)==null);
    }

    @Test
    public void testFetchAll() {
        try {
            assertFalse(tagDAO.getAll().isEmpty());
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }

    @Test
    public void testFetchByNewsId() {

        try {
            List<Tag> tags = tagDAO.fetchByNewsId(2L);
            assertFalse(tags.isEmpty());

        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }

    @Test(expected = NullPointerException.class)
    public void testFetchByNullNewsId() {

        try {
            tagDAO.fetchByNewsId(null);
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }

    @Test
    public void testUnlinkTagFromAllNews() {
        try {
            tagDAO.unlinkTagFromAllNews(2L);
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }

    @Test(expected = NullPointerException.class)
    public void testUnlinkNullTagFromAllNews() {
        try {
            tagDAO.unlinkTagFromAllNews(null);
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }

    @Test
    public void testUpdate() {
        tag = new Tag(null, "Test tag");
        try {
            Long id = tagDAO.insert(tag);
            tag.setId(id);
            tag.setTag("Updated_tag");
            tagDAO.update(tag);
            assertEquals(tag.getTag(), tagDAO.fetch(id).getTag());
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }

    @Test(expected = DAOException.class)
    public void testUpdateWithNull() throws DAOException {
        tag = new Tag(null, "Test tag");
        Long id = tagDAO.insert(tag);
        tag.setId(id);
        tag.setTag(null);
        tagDAO.update(tag);
        assertEquals(tag.getTag(), tagDAO.fetch(id).getTag());

    }

    @Test
    public void testDeleteTag() {
        tag = new Tag(null, "Test tag");
        try {
            Long id = tagDAO.insert(tag);
            tag.setId(id);
            tagDAO.delete(id);
            assertTrue(tagDAO.exist(tag) == null);
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }

    @Test(expected = NullPointerException.class)
    public void testDeleteNullTag() {
        try {
            tagDAO.delete(null);
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }


}

