package com.epam.newsManagerApp.daotest;

import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.dao.impl.NewsDAOImpl;
import com.epam.newsManagerApp.dao.query.QueryGenerator;
import com.epam.newsManagerApp.domain.News;
import com.epam.newsManagerApp.domain.Tag;
import com.epam.newsManagerApp.transfer.search.NewsSearchCriteria;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Artsiom_Yurhevich on 6/24/2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-context.xml")
@DatabaseSetup("classpath:/testdata/NewsData.xml")
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
@DatabaseTearDown(value = "classpath:/testdata/NewsData.xml", type = DatabaseOperation.DELETE_ALL)

public class NewsDAOTest {

    //date of update ??????

    @Autowired
    NewsDAOImpl newsDAO;


    private News news;
    private List<Tag> tags;


    @Before
    public void init() {
        tags = new ArrayList<>();
    }

    @Test
    public void testInsertNews() {
        news = new News(null, "test title", "sort text", "full text", LocalDateTime.now(), LocalDateTime.now());
        try {
            assertTrue(newsDAO.insert(news) > 0);
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }

    @Test(expected = DAOException.class)
    public void testInsertNullNews() throws DAOException {
        news = new News(null, "test title", null, null, LocalDateTime.now(), LocalDateTime.now());
        newsDAO.insert(news);

    }

    @Test
    @Transactional(rollbackFor = Exception.class)
    public void testFetch() {
        news = new News(null, "test title", "sort text", "full text", LocalDateTime.now(), LocalDateTime.now());
        try {
            Long id = newsDAO.insert(news);
            news.setId(id);
            assertEquals(news, newsDAO.fetch(id));
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }


    @Test
    public void testUpdateNews() {

        news = new News(null, "test title", "sort text", "full text", LocalDateTime.now(), LocalDateTime.now());

        try {
            Long id = newsDAO.insert(news);
            news.setId(id);
            News expected = new News(id, "update_title", "update_short", "update_full", LocalDateTime.now().plusMonths(2), LocalDateTime.now().minusMinutes(3));
            newsDAO.update(expected);
            News updated = newsDAO.fetch(id);
            assertEquals(expected.getFullText(), updated.getFullText());

        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }


    @Test
    public void testDeleteNews() {
        news = new News(null, "test title", "sort text", "full text", LocalDateTime.now(), LocalDateTime.now());

        try {
            Long id = newsDAO.insert(news);
            newsDAO.delete(id);
            assertNull(newsDAO.fetch(id));

        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }


    @Test
    public void testCountAllNews() {
        try {
            assertTrue(newsDAO.countNews() > 0);
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }
    @Test
    public void testCountSearchNews() {

        NewsSearchCriteria searchCriteria = new NewsSearchCriteria();
        List<String> tags = new ArrayList<>();
        tags.add("test_tag_1");
        List<String> authors = new ArrayList<>();
        authors.add("test_author_2");
        searchCriteria.setAuthors(authors);
        searchCriteria.setTags(tags);


        int count;
        try {
            count = newsDAO.countNews(searchCriteria);
            System.out.println(count);
            assertTrue(newsDAO.countNews(searchCriteria) > 0);
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }

    @Test
    public void testLinkToAuthor() {
        news = new News(null, "test title", "sort text", "full text", LocalDateTime.now(), LocalDateTime.now());

        try {
            Long id = newsDAO.insert(news);
            newsDAO.linkToAuthor(id, 2L);

        } catch (DAOException e) {
            fail("DAO execution failed");
        }

    }

    @Test(expected = DAOException.class)
    public void testLinkToNotExistingAuthor() throws DAOException {
        news = new News(null, "test title", "sort text", "full text", LocalDateTime.now(), LocalDateTime.now());
        Long id = newsDAO.insert(news);
        newsDAO.linkToAuthor(id, 1000L);

    }

    @Test
    public void testGetNews() {

        NewsSearchCriteria searchCriteria = new NewsSearchCriteria();
     

        List<String> tags = new ArrayList<>();
        tags.add("test_tag_1");
        List<String> authors = new ArrayList<>();
        authors.add("test_author_2");
        searchCriteria.setAuthors(authors);
        searchCriteria.setTags(tags);

        try {
            List<News> list = newsDAO.getNews(searchCriteria);
            assertTrue(list.size() > 0);
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }


    @Test
    public void testLinkToTags() {

        news = new News(null, "test title", "sort text", "full text", LocalDateTime.now(), LocalDateTime.now());

        try {
            Long id = newsDAO.insert(news);
            newsDAO.linkToTags(id, new ArrayList<Long>() {{
                add(2L);
                add(3L);
            }});

        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }


    @Test(expected = DAOException.class)
    public void testLinkToNotExistingTags() throws DAOException {

        news = new News(null, "test title", "sort text", "full text", LocalDateTime.now(), LocalDateTime.now());
        Long id = newsDAO.insert(news);
        newsDAO.linkToTags(id, new ArrayList<Long>() {{
            add(1L);
            add(10000L);
        }});
    }


    @Test
    public void testUnlinkAuthor(){
        try {
            newsDAO.unlinkAuthor(1L);
        } catch (DAOException e) {
            fail("DAO execution failed" + e.getMessage());
        }
    }


    @Test
    public void testUnlinkTags(){
        try {
            newsDAO.unlinkTags(1L);
        } catch (DAOException e) {
            fail("DAO execution failed");
        }
    }


}




