package com.epam.newsManagerApp.servicetest;

import com.epam.newsManagerApp.dao.NewsDAO;
import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.domain.News;
import com.epam.newsManagerApp.service.exception.ServiceException;
import com.epam.newsManagerApp.service.impl.NewsServiceImpl;
import com.epam.newsManagerApp.transfer.search.NewsSearchCriteria;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

/**
 * Created by Comp on 26.06.2016.
 */

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {

    @Mock
    private NewsDAO newsDaoMock;

    @InjectMocks
    private NewsServiceImpl newsServiceMock;


    private News news;
    private List<News> newsList;
    private Long testId;

    @Before
    public void init() {
        news = new News(null, "test title", "test short", "test full", LocalDateTime.now(), LocalDateTime.now());
        newsList = new ArrayList<>();
        newsList.add(news);
        testId = 1L;
    }


    @Test
    public void testGetSingleNews() throws DAOException, ServiceException {
        when(newsDaoMock.fetch(anyLong())).thenReturn(news);
        News fetchedNews = newsServiceMock.getNews(testId);
        assertEquals(news, fetchedNews);
        verify(newsDaoMock, times(1)).fetch(testId);
    }


    @Test(expected = ServiceException.class)
    public void testGetSingleNewsHandleDAOException() throws DAOException, ServiceException {
        when(newsDaoMock.fetch(anyLong())).thenThrow(new DAOException());
        newsServiceMock.getNews(testId);
    }


    @Test
    public void testGetListNews() throws DAOException, ServiceException {
        NewsSearchCriteria searchCriteria = new NewsSearchCriteria();
        when(newsDaoMock.getNews(any())).thenReturn(newsList);
        List<News> fetchedList = newsServiceMock.getNews(searchCriteria);
        assertEquals(newsList, fetchedList);
        verify(newsDaoMock, times(1)).getNews(searchCriteria);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetListNewsNullSearch() throws DAOException, ServiceException {
        NewsSearchCriteria searchCriteria = null;
        newsServiceMock.getNews(searchCriteria);
    }

    @Test(expected = ServiceException.class)
    public void testGetListNewsHandleDAOException() throws DAOException, ServiceException {
        when(newsDaoMock.getNews(any())).thenThrow(new DAOException());
        newsServiceMock.getNews(new NewsSearchCriteria());
    }


    @Test
    public void testAddNews() throws DAOException, ServiceException {
        when(newsDaoMock.insert(news)).thenReturn(testId);
        Long id = newsDaoMock.insert(news);
        assertEquals(testId, id);
        verify(newsDaoMock, times(1)).insert(news);
    }


    @Test(expected = ServiceException.class)
    public void testAddNewsHandleDAOException() throws DAOException, ServiceException {
        when(newsDaoMock.insert(any())).thenThrow(new DAOException());
        newsServiceMock.addNews(news);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddNullComment() throws DAOException, ServiceException {
        newsServiceMock.addNews(null);
    }

    @Test
    public void testDeleteNews() throws DAOException, ServiceException {
        newsServiceMock.deleteNews(testId);
        verify(newsDaoMock, times(1)).delete(testId);
    }

    @Test(expected = ServiceException.class)
    public void testDeleteNewsHandleDAOException() throws DAOException, ServiceException {
        Mockito.doThrow(new DAOException()).when(newsDaoMock).delete(anyLong());
        newsServiceMock.deleteNews(testId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteNullNews() throws DAOException, ServiceException {
        newsServiceMock.deleteNews(null);
    }

    @Test
    public void testLinkAuthor() throws DAOException, ServiceException {
        newsServiceMock.linkAuthor(testId, testId);
        verify(newsDaoMock, times(1)).linkToAuthor(testId, testId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLinkNullAuthor() throws DAOException, ServiceException {
        newsServiceMock.linkAuthor(testId, null);
    }

    @Test
    public void testLinkTags() throws DAOException, ServiceException {
        List<Long> tagIdList = new ArrayList<>();
        tagIdList.add(2L);
        newsServiceMock.linkTags(testId, tagIdList);
        verify(newsDaoMock, times(1)).linkToTags(testId, tagIdList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLinkNullTags() throws DAOException, ServiceException {
        newsServiceMock.linkTags(testId, null);
    }

    @Test
    public void testUnlinkAuthor() throws DAOException, ServiceException {
        newsServiceMock.unlinkAuthor(testId);
        verify(newsDaoMock, times(1)).unlinkAuthor(testId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUnlinkNullAuthor() throws DAOException, ServiceException {
        newsServiceMock.unlinkAuthor(null);
    }

    @Test
    public void testUnlinkTag() throws DAOException, ServiceException {
        newsServiceMock.unlinkTags(testId);
        verify(newsDaoMock, times(1)).unlinkTags(testId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUnlinkNullTag() throws DAOException, ServiceException {
        newsServiceMock.unlinkTags(null);
    }

    @Test
    public void testEditNews() throws DAOException, ServiceException {
        newsServiceMock.editNews(news);
        verify(newsDaoMock, times(1)).update(news);
    }

    @Test(expected = ServiceException.class)
    public void testEditNewsHandleDAOException() throws DAOException, ServiceException {
        Mockito.doThrow(new DAOException()).when(newsDaoMock).update(any());
        newsServiceMock.editNews(news);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEditNullNews() throws DAOException, ServiceException {
        newsServiceMock.editNews(null);
    }

    @Test
    public void testCountAllNews() throws DAOException, ServiceException {
        when(newsDaoMock.countNews()).thenReturn(22);
        int fetchedCount = newsServiceMock.countNews();
        assertTrue(fetchedCount == 22);
        verify(newsDaoMock, times(1)).countNews();
    }


}
