package com.epam.newsManagerApp.servicetest;

import com.epam.newsManagerApp.dao.TagDAO;
import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.domain.Tag;
import com.epam.newsManagerApp.service.exception.ServiceException;
import com.epam.newsManagerApp.service.impl.TagServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

/**
 * Created by Artsiom_Yurhevich on 6/28/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {

    @Mock
    private TagDAO tagDaoMock;

    @InjectMocks
    private TagServiceImpl tagService;

    private Tag tag;
    private List<Tag> tagList;
    private Long testId;

    @Before
    public void init() {
        tag = new Tag(null, "test tag 1");
        tagList = new ArrayList<>();
        tagList.add(tag);
        tagList.add(new Tag(null, "test tag 2"));
        testId = 1L;
    }

    @Test
    public void testGetOrCreateSingle() throws DAOException, ServiceException {
        when(tagDaoMock.exist(tag)).thenReturn(null);
        when(tagDaoMock.insert(tag)).thenReturn(1L);
        Long id = tagService.getOrCreate(tag);
        assertEquals(testId, id);
        verify(tagDaoMock, times(1)).exist(tag);
        verify(tagDaoMock, times(1)).insert(tag);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetOrCreateSingleNullTag() throws DAOException, ServiceException {
        Tag nullTag = null;
        tagService.getOrCreate(nullTag);
    }

    @Test(expected = ServiceException.class)
    public void testGetOrCreateSingleTagHandleDAOException() throws DAOException, ServiceException {
        when(tagDaoMock.exist(tag)).thenReturn(null);
        when(tagDaoMock.insert(any())).thenThrow(new DAOException());
        tagService.getOrCreate(tag);
    }

    @Test
    public void testGetOrCreateList() throws DAOException, ServiceException {
        when(tagDaoMock.exist(any())).thenReturn(null);
        when(tagDaoMock.insert(any())).thenReturn(1L);
        List<Long> listId = tagService.getOrCreate(tagList);
        assertTrue(listId.size() == tagList.size());
        verify(tagDaoMock, times(tagList.size())).exist(any());
        verify(tagDaoMock, times(tagList.size())).insert(any());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetOrCreateNullList() throws DAOException, ServiceException {
        List<Tag> nullList = null;
        tagService.getOrCreate(nullList);
    }

    @Test(expected = ServiceException.class)
    public void testGetOrCreateTagListHandleDAOException() throws DAOException, ServiceException {
        when(tagDaoMock.exist(tag)).thenThrow(new DAOException());
        tagService.getOrCreate(tagList);
    }

    @Test
    public void testFetchAll() throws DAOException, ServiceException {
        when(tagDaoMock.getAll()).thenReturn(tagList);
        List<Tag> fetchedList = tagService.getAll();
        assertEquals(tagList, fetchedList);
        verify(tagDaoMock, times(1)).getAll();
    }

    @Test(expected = ServiceException.class)
    public void testFetchAllDAOException() throws DAOException, ServiceException {
        when(tagDaoMock.getAll()).thenThrow(new DAOException());
        tagService.getAll();
    }

    @Test
    public void testEditTag() throws DAOException, ServiceException {
        tagService.editTag(tag);
        verify(tagDaoMock, times(1)).update(tag);
    }

    @Test(expected = ServiceException.class)
    public void testEditTagHandleDAOException() throws DAOException, ServiceException {
        doThrow(new DAOException()).when(tagDaoMock).update(any());
        tagService.editTag(tag);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEditNullTag() throws ServiceException {
        tagService.editTag(null);
    }

    @Test
    public void testDeleteTag() throws DAOException, ServiceException {
        tagService.deleteTag(testId);
        verify(tagDaoMock, times(1)).delete(testId);
    }

    @Test(expected = ServiceException.class)
    public void testDeleteHandleDAOException() throws DAOException, ServiceException {
        Mockito.doThrow(new DAOException()).when(tagDaoMock).delete(any());
        tagService.deleteTag(testId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteNullTag() throws ServiceException {
        tagService.deleteTag(null);
    }


    @Test
    public void testGetNewsTags() throws DAOException, ServiceException {
        when(tagDaoMock.fetchByNewsId(anyLong())).thenReturn(tagList);
        List<Tag> fetchedList = tagService.getNewsTags(testId);
        assertEquals(tagList, fetchedList);
        verify(tagDaoMock, times(1)).fetchByNewsId(testId);
    }

    @Test(expected = ServiceException.class)
    public void testGetNewsTagsDAOException() throws DAOException, ServiceException {
        when(tagDaoMock.fetchByNewsId(anyLong())).thenThrow(new DAOException());
        tagService.getNewsTags(testId);
    }

    @Test
    public void testUnlinkTagFromAllNews() throws DAOException, ServiceException {
        tagService.unlinkTagFromAllNews(testId);
        verify(tagDaoMock, times(1)).unlinkTagFromAllNews(testId);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testUnlinkNullTagFromAllNews() throws ServiceException {
        tagService.unlinkTagFromAllNews(null);
    }

}
