package com.epam.newsManagerApp.servicetest;

import com.epam.newsManagerApp.dao.AuthorDAO;
import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.domain.Author;
import com.epam.newsManagerApp.service.exception.ServiceException;
import com.epam.newsManagerApp.service.impl.AuthorServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by Artsiom_Yurhevich on 6/27/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {

    @Mock
    private AuthorDAO authorDAOMock;

    @InjectMocks
    private AuthorServiceImpl serviceMock;

    private Author author;
    private Long testId;

    @Before
    public void init() {
        author = new Author(null, "test_name", null);
        testId = 1L;
    }

    @Test
    public void testGetOrCreate() throws DAOException, ServiceException {
        when(authorDAOMock.exist(author)).thenReturn(null);
        when(authorDAOMock.insert(author)).thenReturn(testId);
        Long id = serviceMock.getOrCreate(author);
        assertEquals(testId, id);
        verify(authorDAOMock, times(1)).exist(author);
        verify(authorDAOMock, times(1)).insert(author);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetOrCreateNullAuthor() throws DAOException, ServiceException {
        serviceMock.getOrCreate(null);
    }

    @Test(expected = ServiceException.class)
    public void testGetOrCreateHandleException() throws DAOException, ServiceException {
        when(authorDAOMock.exist(author)).thenReturn(null);
        Mockito.doThrow(new DAOException()).when(authorDAOMock).insert(any());
        serviceMock.getOrCreate(author);
    }


    @Test
    public void testSetExpired() throws DAOException, ServiceException {
        serviceMock.setExpired(author);
        verify(authorDAOMock, times(1)).setExpirationDate(author);
    }

    @Test(expected = ServiceException.class)
    public void testSetExpiredHandleException() throws DAOException, ServiceException {
        Mockito.doThrow(new DAOException()).when(authorDAOMock).setExpirationDate(any());
        serviceMock.setExpired(author);
    }

    @Test
    public void testEditAuthor() throws DAOException, ServiceException {
        serviceMock.editAuthor(author);
        verify(authorDAOMock, times(1)).update(author);
    }

    @Test(expected = ServiceException.class)
    public void testEditAuthorHandleException() throws DAOException, ServiceException {
        Mockito.doThrow(new DAOException()).when(authorDAOMock).update(any());
        serviceMock.editAuthor(author);
    }

    @Test
    public void testFetchAll() throws DAOException, ServiceException {
        List<Author> listAuth = new ArrayList<Author>();
        listAuth.add(author);
        when(authorDAOMock.fetchAll()).thenReturn(listAuth);
        List<Author> fetchedList = serviceMock.getAll();
        verify(authorDAOMock, times(1)).fetchAll();
        assertEquals(listAuth, fetchedList);
    }


    @Test(expected = ServiceException.class)
    public void testFetchAllDAOException() throws DAOException, ServiceException {
        when(authorDAOMock.fetchAll()).thenThrow(new DAOException());
        serviceMock.getAll();
    }

    @Test
    public void testFetchNewsAuthor() throws DAOException, ServiceException {
        when(authorDAOMock.getNewsAuthor(testId)).thenReturn(author);
        Author fetched = serviceMock.getNewsAuthor(testId);
        verify(authorDAOMock, times(1)).getNewsAuthor(testId);
        assertEquals(author, fetched);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFetchNullNewsAuthor() throws DAOException, ServiceException {
        serviceMock.getNewsAuthor(null);
    }


}
