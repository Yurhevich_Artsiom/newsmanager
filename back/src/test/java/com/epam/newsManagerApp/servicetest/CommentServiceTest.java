package com.epam.newsManagerApp.servicetest;

import com.epam.newsManagerApp.dao.CommentDAO;
import com.epam.newsManagerApp.dao.exception.DAOException;
import com.epam.newsManagerApp.domain.Comment;
import com.epam.newsManagerApp.service.exception.ServiceException;
import com.epam.newsManagerApp.service.impl.CommentServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by Artsiom_Yurhevich on 6/27/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {

    @Mock
    private CommentDAO commentDAOMock;

    @InjectMocks
    private CommentServiceImpl commentService;

    private Comment comment;
    private Long testId;

    @Before
    public void init() {
        comment = new Comment(null, 1L, "comment text", LocalDateTime.now());
        testId = 1L;
    }


    @Test
    public void testAddComment() throws DAOException, ServiceException {
        when(commentDAOMock.insert(comment)).thenReturn(testId);
        Long id = commentDAOMock.insert(comment);
        assertEquals(testId, id);
        verify(commentDAOMock, times(1)).insert(comment);
    }


    @Test(expected = ServiceException.class)
    public void testAddCommentHandleDAOException() throws DAOException, ServiceException {
        when(commentDAOMock.insert(any())).thenThrow(new DAOException());
        commentService.addComment(comment);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddNullComment() throws DAOException, ServiceException {
        commentService.addComment(null);
    }


    @Test
    public void testDeleteComment() throws DAOException, ServiceException {
        commentService.deleteComment(testId);
        verify(commentDAOMock, times(1)).delete(testId);
    }


    @Test(expected = ServiceException.class)
    public void testDeleteCommentHandleDAOException() throws DAOException, ServiceException {
        Mockito.doThrow(new DAOException()).when(commentDAOMock).delete(any());
        commentService.deleteComment(testId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteNullComment() throws DAOException, ServiceException {
        commentService.deleteComment(null);
    }


    @Test
    public void testGetNewsComments() throws DAOException, ServiceException {
        List<Comment> listCmt = new ArrayList<>();
        listCmt.add(comment);
        when(commentDAOMock.getNewsComments(testId)).thenReturn(listCmt);
        List<Comment> fetched = commentService.getNewsComments(testId);
        verify(commentDAOMock, times(1)).getNewsComments(testId);
        assertEquals(listCmt, fetched);
    }


    @Test(expected = ServiceException.class)
    public void testGetNewsCommentsHandleDAOException() throws DAOException, ServiceException {
        when(commentDAOMock.getNewsComments(anyLong())).thenThrow(new DAOException());
        commentService.getNewsComments(anyLong());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetNullNewsComments() throws DAOException, ServiceException {
        commentService.getNewsComments(null);
    }

    @Test
    public void testDeleteNewsComments() throws DAOException, ServiceException {
        commentService.deleteNewsComments(testId);
        verify(commentDAOMock, times(1)).deleteNewsComments(testId);
    }


    @Test(expected = ServiceException.class)
    public void testDeleteNewsCommentsHandleDAOException() throws DAOException, ServiceException {
        Mockito.doThrow(new DAOException()).when(commentDAOMock).deleteNewsComments(any());
        commentService.deleteNewsComments(testId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteNullNewsComments() throws DAOException, ServiceException {
        commentService.deleteNewsComments(null);
    }


}
