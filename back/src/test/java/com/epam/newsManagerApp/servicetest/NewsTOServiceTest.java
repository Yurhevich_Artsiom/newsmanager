package com.epam.newsManagerApp.servicetest;

import com.epam.newsManagerApp.domain.Author;
import com.epam.newsManagerApp.domain.Comment;
import com.epam.newsManagerApp.domain.News;
import com.epam.newsManagerApp.domain.Tag;
import com.epam.newsManagerApp.service.exception.ServiceException;
import com.epam.newsManagerApp.service.impl.*;
import com.epam.newsManagerApp.transfer.NewsTransferObject;
import com.epam.newsManagerApp.transfer.search.NewsSearchCriteria;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by Comp on 03.07.2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsTOServiceTest {

    @Mock
    private NewsServiceImpl newsServiceMock;

    @Mock
    private AuthorServiceImpl authorServiceMock;

    @Mock
    private TagServiceImpl tagServiceMock;

    @Mock
    private CommentServiceImpl commentServiceMock;

    @InjectMocks
    private NewsTOServiceImpl newsTOService;

    NewsTransferObject newsTO;
    News news;
    Author author;
    List<Tag> tagList;
    List<Comment> commentList;
    List<News> newsList;
    Long testId;

    @Before
    public void init() {
        testId = 1L;
        news = new News(testId, "title", "short", "full", LocalDateTime.now(), LocalDateTime.now());
        author = new Author(null, "name", null);
        tagList = new ArrayList<>();
        tagList.add(new Tag(null, "tag"));
        commentList = new ArrayList<>();
        commentList.add(new Comment(null, null, "comment", LocalDateTime.now()));
        newsList = new ArrayList<>();
        newsList.add(news);
        newsList.add(new News());
        newsTO = new NewsTransferObject();
        newsTO.setNews(news);
        newsTO.setAuthor(author);
        newsTO.setTags(tagList);
    }

    @Test
    public void testGetSingleNewsTO() throws ServiceException {
        when(newsServiceMock.getNews(anyLong())).thenReturn(news);
        when(authorServiceMock.getNewsAuthor(anyLong())).thenReturn(author);
        when(tagServiceMock.getNewsTags(anyLong())).thenReturn(tagList);
        when(commentServiceMock.getNewsComments(anyLong())).thenReturn(commentList);

        NewsTransferObject fetchedNewsTO = newsTOService.getNewsTransferObject(testId);

        assertEquals(news, fetchedNewsTO.getNews());
        assertEquals(author, fetchedNewsTO.getAuthor());
        assertEquals(tagList, fetchedNewsTO.getTags());
        assertEquals(commentList, fetchedNewsTO.getComments());

        verify(newsServiceMock, times(1)).getNews(testId);
        verify(authorServiceMock, times(1)).getNewsAuthor(testId);
        verify(tagServiceMock, times(1)).getNewsTags(testId);
        verify(commentServiceMock, times(1)).getNewsComments(testId);

    }


    @Test
    public void testGetSingleNotExistingNewsTO() throws ServiceException {
        when(newsServiceMock.getNews(anyLong())).thenReturn(null);

        NewsTransferObject fetchedNewsTO = newsTOService.getNewsTransferObject(testId);

        assertNull(fetchedNewsTO);

        verify(newsServiceMock, times(1)).getNews(testId);
        verify(authorServiceMock, never()).getNewsAuthor(anyLong());
        verify(tagServiceMock, never()).getNewsTags(anyLong());
        verify(commentServiceMock, never()).getNewsComments(anyLong());

    }

    @Test(expected = ServiceException.class)
    public void testGetSingleNewsTOHandleException() throws ServiceException {
        when(newsServiceMock.getNews(anyLong())).thenReturn(news);
        when(authorServiceMock.getNewsAuthor(anyLong())).thenThrow(new ServiceException());
        newsTOService.getNewsTransferObject(testId);

    }


    @Test
    public void testGetListNewsTO() throws ServiceException {
        NewsSearchCriteria searchCriteria = new NewsSearchCriteria();

        when(newsServiceMock.getNews(searchCriteria)).thenReturn(newsList);
        when(authorServiceMock.getNewsAuthor(anyLong())).thenReturn(author);
        when(tagServiceMock.getNewsTags(anyLong())).thenReturn(tagList);
        when(commentServiceMock.getNewsComments(anyLong())).thenReturn(commentList);

        List<NewsTransferObject> fetchedListNewsTO = newsTOService.getNewsTransferObject(searchCriteria);

        assertTrue(fetchedListNewsTO.size() == newsList.size());


        assertEquals(news, fetchedListNewsTO.get(0).getNews());
        assertEquals(author, fetchedListNewsTO.get(0).getAuthor());
        assertEquals(tagList, fetchedListNewsTO.get(0).getTags());
        assertEquals(commentList, fetchedListNewsTO.get(0).getComments());

        verify(newsServiceMock, times(1)).getNews(searchCriteria);
        verify(authorServiceMock, times(newsList.size())).getNewsAuthor(anyLong());
        verify(tagServiceMock, times(newsList.size())).getNewsTags(anyLong());
        verify(commentServiceMock, times(newsList.size())).getNewsComments(anyLong());

    }


    @Test(expected = IllegalArgumentException.class)
    public void testGetListNewsTONullSearch() throws ServiceException {
        NewsSearchCriteria searchCriteria = null;
        newsTOService.getNewsTransferObject(searchCriteria);
    }

    @Test
    public void addNews() throws ServiceException {
        List<Tag> tags = new ArrayList<>();
        List<Long> tagId = new ArrayList<>();
        when(newsServiceMock.addNews(any())).thenReturn(testId);
        when(authorServiceMock.getId(any())).thenReturn(testId);
        when(tagServiceMock.getId(tagList)).thenReturn(tagId);

        newsTOService.addNews(newsTO);

        verify(newsServiceMock, times(1)).addNews(newsTO.getNews());
        verify(authorServiceMock, times(1)).getId(newsTO.getAuthor());
        verify(tagServiceMock, times(1)).getId(newsTO.getTags());
        verify(newsServiceMock, times(1)).linkAuthor(testId, testId);
        verify(newsServiceMock, times(1)).linkTags(testId, tagId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addNullNews() throws ServiceException {
        newsTOService.addNews(null);
    }


    @Test
    public void editNews() throws ServiceException {
        List<Long> tagId = new ArrayList<>();

        when(authorServiceMock.getId(any())).thenReturn(testId);
        when(tagServiceMock.getId(tagList)).thenReturn(tagId);

        newsTOService.editNews(newsTO);

        verify(newsServiceMock, times(1)).editNews(newsTO.getNews());
        verify(newsServiceMock, times(1)).unlinkAuthor(newsTO.getNews().getId());
        verify(authorServiceMock, times(1)).getId(newsTO.getAuthor());
        verify(newsServiceMock, times(1)).linkAuthor(testId, testId);
        verify(newsServiceMock, times(1)).unlinkTags(newsTO.getNews().getId());
        verify(tagServiceMock, times(1)).getId(newsTO.getTags());
        verify(newsServiceMock, times(1)).linkTags(testId, tagId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void editNullNews() throws ServiceException {
        newsTOService.editNews(null);
    }

    @Test
    public void deleteNews() throws ServiceException {

        newsTOService.deleteNews(testId);

        verify(newsServiceMock, times(1)).unlinkAuthor(testId);
        verify(newsServiceMock, times(1)).unlinkTags(testId);
        verify(commentServiceMock, times(1)).deleteNewsComments(testId);
        verify(newsServiceMock, times(1)).deleteNews(testId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteNullNews() throws ServiceException {
        newsTOService.deleteNews(null);
    }


}
