<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	
<header>
	<sec:authorize access="hasRole('ROLE_ADMIN')">
		<spring:url var="logoutUrl" value="/logout" />
		<%--<c:url var="logoutUrl"  value="/logout" />--%>
		<form class="logout" action='${logoutUrl}' method='POST'>
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" /> <input type="submit"
				class="sbm-btn logout-btn" value="<spring:message code="logout" />">
		</form>
	</sec:authorize>
	<div class="localization">
		<a
			href="${requestScope['javax.servlet.forward.request_uri']}?locale=en"><spring:message code="localization.en" /></a>
		<a
			href="${requestScope['javax.servlet.forward.request_uri']}?locale=ru"><spring:message code="localization.ru" /></a>
	</div>
		<c:url var="index" value="/news/list" />
	<h1 class="header"><a href="${index}"><spring:message code="title" /></a></h1>
</header>