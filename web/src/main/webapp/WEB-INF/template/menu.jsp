<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<nav>
	<ul class="menubar">
		<li><a href="<c:url value='/news'/>" <c:if test="${active == 'news/list'}">class="active"</c:if>><spring:message code="news.list" /></a></li>
		<li><a href="<c:url value='/news/add'/>" <c:if test="${active == 'news/add'}">class="active"</c:if>><spring:message code="news.add" /></a></li>
		<li><a href="<c:url value='/author/list'/>" <c:if test="${active == 'author/list'}">class="active"</c:if>><spring:message code="authors.update" /></a></li>
		<li><a href="<c:url value='/tag/list'/>" <c:if test="${active == 'tag/list'}">class="active"</c:if>><spring:message code="tags.update" /></a></li>
	</ul>
</nav>