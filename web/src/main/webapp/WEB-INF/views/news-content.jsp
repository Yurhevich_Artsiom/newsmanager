<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="news-content-container">
	<div class="news-header">
		<div class="news-header_title">
			<h3>${newsTO.news.title}</h3>
		</div>
		<div class="news-additional-title">
			<div class="author">${newsTO.author.name}</div>
			<div class="date">${newsTO.news.creationDate.toLocalDate()}</div>
		</div>
	</div>

	<div class="news-content">${newsTO.news.fullText}</div>

	<c:url var='orderNumber' value='/news/orderNumber' />
	<div class="next-prev">
		<div class="previous">&nbsp
			<c:if test="${not empty nextPrev[0]}">
			<a href="${orderNumber}?orderNumber=${nextPrev[0]}" ><spring:message code="prev"/></a>
			</c:if>
		</div>
		<div class="next">
		<c:if test="${not empty nextPrev[1]}">
			<a href="${orderNumber}?orderNumber=${nextPrev[1]}" ><spring:message code="next"/></a>
			</c:if>
		</div>
	</div>



	<div class="comments-container" id="comments">

		<c:url var='deleteUrl' value='/news/comment/delete' />

		<c:forEach items="${newsTO.comments}" var="comment">

			<div class="comment">
				<div class="comment-date">
					${comment.creationDate.toLocalDate()}
					${comment.creationDate.getHour()}:${comment.creationDate.getMinute()}</div>
				<div class="comment-text">${comment.comment}</div>
				<div class="comment-edit">
					<sf:form action="${deleteUrl}" method="POST">
						<input type="text" name="idComment" value="${comment.id}"
							hidden="true" />
						<input type="text" name="idNews" value="${newsTO.news.id}"
							hidden="true" />
						<button class="delete delete-comment" type="submit"><spring:message code="delete"/></button>
					</sf:form>
				</div>
			</div>

		</c:forEach>


		<div class="new-comment" id="new">

			<c:url var="postUrl" value="/news/comment/post" />
			<sf:form method="POST" action="${postUrl}" modelAttribute="comment">
				<sf:input path="idNews" value="${newsTO.news.id}" hidden="true" />
				<div class="comment-text">
					<p>
						<sf:errors path="comment" cssClass="error" />
					</p>
					<sf:textarea path="comment" class="new-cmn-text" maxlength="100"
						minlength="3"  required="true"></sf:textarea>
				</div>
				<div class="post-comment">
					<button type="submit" class="sbm-btn">
						<span><spring:message code="post"/></span>
					</button>
				</div>
			</sf:form>
		</div>
	</div>
</div>