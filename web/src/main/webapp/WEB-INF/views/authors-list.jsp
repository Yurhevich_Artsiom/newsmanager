<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="author-container">

	<c:url var='deleteUrl' value='/author/delete' />
	<c:url var='editUrl' value='/author/edit' />
	<p id="edit-url" hidden="hidden">${editUrl}</p>
	<c:url var='createUrl' value='/author/create' />
	<p id="create-url" hidden="hidden">${createUrl}</p>

	<ul class="authors">
	
				<c:forEach var="auth" items="${authorList}">
					<li><p>${auth.name}</p>

						<button class="round-btn edit-item" type="submit">
							<spring:message code="edit" />
						</button> <sf:form action="${deleteUrl}" method="POST">
							<input type="text" name="id" value="${auth.id}" hidden="true" />
							<input type="text" name="page" value="${cur_page}" hidden="true" />
							<button class="delete-author round-btn" type="submit">
								<spring:message code="delete" />
							</button>
						</sf:form></li>
				</c:forEach>
	
		<li>
			<button class="create edit" id="create" type="submit">
				<spring:message code="create" />
			</button>
	</ul>
	<div class="pagination-container">
		<ul class="pagination">
			<c:if test="${max_page ne 1}">
				<c:if test="${left_switch != null}">

					<li><a href="<c:url value='/author?page=${left_switch}'/>">«</a></li>
				</c:if>
				<c:forEach begin="${min_page}" end="${max_page}" var="val">
					<c:choose>
						<c:when test="${cur_page == val}">
							<li><a href="<c:url value='/author?page=${val}'/>"
								class="active">${val}</a></li>
						</c:when>
						<c:otherwise>
							<li><a href="<c:url value='/author?page=${val}'/>">${val}</a></li>
						</c:otherwise>
					</c:choose>
				</c:forEach>
				<c:if test="${right_switch != null}">
					<li><a href="<c:url value='/author?page=${right_switch}'/>">»</a></li>
				</c:if>

			</c:if>
		</ul>
	</div>

	<div id="edit-modal" class="modal">
		<div class="modal-content">
			<form action="" id="edit" method="POST">

				<div class="modal-header">
					<span class="close" id="close-edit_modal">X</span>

					<h2>
						<spring:message code="author.name" />
					</h2>
				</div>
				<div class="modal-body">
					<p>
						<spring:message code="author.input.name" />
					</p>
					<p id="edit-message" style="color: red"></p>
					<input type="text" minlength="3" maxlength="30" name="name"
						placeholder="<spring:message code="author.name" />" required /> <input
						type="text" name="id" value="" hidden="true" />
				</div>
				<div class="modal-footer">
					<button type="submit" class="sbm-btn">
						<span><spring:message code="post" /></span>
					</button>
				</div>
			</form>
		</div>

	</div>

</div>