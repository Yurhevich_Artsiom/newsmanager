<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Artsiom_Yurhevich
  Date: 7/28/2016
  Time: 2:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
	isErrorPage="true"%>
<html>
<head>
<title><spring:message code="error.title" /></title>
</head>
<body>
	<h1><spring:message code="error.page" /></h1>

	<p><spring:message code="error.message" /></p>

	<c:url var="backMain" value="/news/list" />
	<a href="${backMain}"><spring:message code="error.main" /></a>
	<!--
    Failed URL: ${url}
    Exception:  ${exception.message}
        <c:forEach items="${exception.stackTrace}" var="ste">    ${ste}
    </c:forEach>
  -->
</body>
</html>
