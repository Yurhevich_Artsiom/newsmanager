<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="conformation-modal" class="modal">
	<!-- Modal content -->
	<div class="modal-content">
		<div class="modal-header">
			<span class="close" id="close-conf_modal">X</span>

			<h2><spring:message code="conf.delete" /></h2>
		</div>
		<div class="modal-body">
			<p id="conf-message"></p>
		</div>
		<div class="modal-footer">
			<button id="confirm" type="submit" class="sbm-btn">
				<span><spring:message code="yes" /></span>
			</button>
			<button type="submit" id="cancel" class="sbm-btn">
				<span><spring:message code="cancel" /></span>
			</button>
		</div>
	</div>
</div>