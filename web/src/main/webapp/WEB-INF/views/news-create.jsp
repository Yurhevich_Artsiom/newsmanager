<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="new-edit-container">

    <c:url var="postUrl" value="/news/post"/>
    <sf:form method="POST" action="${postUrl}" modelAttribute="newsTO">

        <div class="new-title">
            <p><spring:message code="news.title"/> :</p>

            <p>
                <sf:errors path="news.title" cssClass="error"/>
            </p>
            <sf:textarea minlength="3" maxlength="30" path="news.title" required="true"></sf:textarea>
        </div>

        <div class="new-date">
            <p><spring:message code="news.date"/> :</p>

            <p>
                <sf:errors path="news.creationDate" cssClass="error"/>
            </p>
            <sf:input type="datetime-local" path="news.creationDate" required="true"/>
        </div>

        <div class="new-brief">
            <p><spring:message code="news.brief"/> :</p>

            <p>
                <sf:errors path="news.shortText" cssClass="error"/>
            </p>
            <sf:textarea minlength="10" maxlength="100" path="news.shortText" required="true"></sf:textarea>
        </div>
        <div class="new-content
			">
            <p><spring:message code="news.content"/> :</p>

            <p>
                <sf:errors path="news.fullText" cssClass="error"/>
            </p>
            <sf:textarea minlength="50" maxlength="2000" path="news.fullText" required="true"></sf:textarea>
        </div>

        <div class="new-link select" >

            <span><spring:message code="author"/> : </span>
            <sf:select class="demo" path="author.name" required="true">
                <optgroup label="<spring:message code="author"/>">
                    <c:forEach items="${authorList}" var="author">
                        <sf:option value="${author.name}">${author.name}</sf:option>
                    </c:forEach>
                </optgroup>


            </sf:select>
            <span> <spring:message code="tags"/> : </span> <select multiple class="demo" name="tags">
            <c:forEach items="${tagList}" var="tag">
            <option value="${tag.tag}">${tag.tag}</c:forEach>
        </select>


            <p>
                <sf:errors path="tags" cssClass="error"/>
            </p>

        </div>

        <div class="new-save">
            <button type="submit" class="sbm-btn">
                <span><spring:message code="post"/></span>
            </button>
        </div>

    </sf:form>
</div>
