<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://epam.com/jsp/tlds/custom" prefix="custom"%>

<div class="new-edit-container">

	<c:url var="postUrl" value="/news/edit" />
	<sf:form method="POST" action="${postUrl}" modelAttribute="newsTO">

		<sf:input path="news.id" value="${newsTO.news.id}" hidden="true" />

		<div class="new-title">
			<p><spring:message code="news.title"/> :</p>

			<p>
				<sf:errors path="news.title" cssClass="error" />
			</p>
			<sf:textarea maxlength="30" path="news.title"
				value="${newsTO.news.title}" required="true"></sf:textarea>
		</div>

		<div class="new-date">
			<p><spring:message code="news.date"/> :</p>

			<p>
				<sf:errors path="news.creationDate" cssClass="error" />
			</p>
			<p>${newsTO.news.creationDate.toLocalDate()}</p>
		</div>

		<div class="new-brief">
			<p><spring:message code="news.brief"/> :</p>

			<p>
				<sf:errors path="news.shortText" cssClass="error" />
			</p>
			<sf:textarea maxlength="100" path="news.shortText"
				value="${newsTO.news.shortText}" required="true"></sf:textarea>
		</div>
		<div class="new-content
			">
			<p><spring:message code="news.content"/> :</p>

			<p>
				<sf:errors path="news.fullText" cssClass="error" />
			</p>
			<sf:textarea maxlength="2000" path="news.fullText"
				value="${newsTO.news.fullText}" required="true"></sf:textarea>
		</div>

		<div class="new-link select">

			<span><spring:message code="author"/> : </span>
			<sf:select class="demo" path="author.name" required="true">
				<optgroup label="<spring:message code="author"/>">
					<c:forEach items="${authorList}" var="author">
						<c:choose>
							<c:when test="${newsTO.author.name eq author.name}">
								<sf:option value="${author.name}" selected="true">${author.name}</sf:option>
							</c:when>
							<c:otherwise>
								<sf:option value="${author.name}">${author.name}</sf:option>
							</c:otherwise>

						</c:choose>
					</c:forEach>
				</optgroup>


			</sf:select>
			<span> <spring:message code="tags"/> : </span> <select multiple class="demo" name="tags">
				<c:forEach items="${tagList}" var="tag">
					<c:choose>
						<c:when test="${custom:contains(newsTO.tags, tag )}">
							<option value="${tag.tag}" selected="true">${tag.tag}</option>
							<p>${tag.tag}</p>
							<h1>${tag.tag}</h1>
						</c:when>
						<c:otherwise>
							<option value="${tag.tag}">${tag.tag}</option>
						</c:otherwise>

					</c:choose>
				</c:forEach>
			</select>


			<p>
				<sf:errors path="tags" cssClass="error" />
			</p>
	</div>

		<div class="new-save">
			<button type="submit" class="sbm-btn">
				<span><spring:message code="post"/></span>
			</button>
		</div>

	</sf:form>
</div>
