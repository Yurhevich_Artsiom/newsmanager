<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://epam.com/jsp/tlds/custom" prefix="custom"%>

<div class="content-container">

	<div class="search select">

		<spring:url var="newsUrl" value="/news" />

		<sf:form class="search-form" action='${newsUrl}'
			modelAttribute="search" method='GET'>
			<span><spring:message code="searching" />:</span>
			<sf:select class="demo" path="authors" multiple="true">
				<optgroup label="Authors">
					<c:forEach items="${authorList}" var="author">
						<c:choose>
							<c:when
								test="${custom:contains(sessionScope.search.authors, author.name )}">
								<option value="${author.name}" selected="true">${author.name}</option>
							</c:when>
							<c:otherwise>
								<option value="${author.name}">${author.name}</option>
							</c:otherwise>

						</c:choose>

					</c:forEach>

				</optgroup>
			</sf:select>

			<sf:select class="demo" path="tags" multiple="true">

				<optgroup label="Tags">
					<c:forEach items="${tagList}" var="tag">
						<c:choose>
							<c:when
								test="${custom:contains(sessionScope.search.tags, tag.tag )}">
								<option value="${tag.tag}" selected="true">${tag.tag}</option>
							</c:when>
							<c:otherwise>
								<option value="${tag.tag}">${tag.tag}</option>
							</c:otherwise>

						</c:choose>
					</c:forEach>
				</optgroup>
			</sf:select>

			<button type="submit" class="find sbm-btn">
				<span><spring:message code="search" /></span>
			</button>

			<input type="reset" class="reset sbm-btn"
				value="<spring:message code="reset" />">

		</sf:form>
	</div>
	<div class="news-container">
		<c:url var='deleteUrl' value='/news/delete' />
		<c:choose>
			<c:when test="${empty newsList}">
			<h3 style="text-align: center;"><spring:message code="news.appropriate" /></h3>
			</c:when>
			<c:otherwise>
				<c:forEach var="elem" items="${newsList}">
					<div class="news">
						<div class="news-info">
							<div class="news-title">
								<a href="<c:url value='/news/view/${elem.news.id}'/>">${elem.news.title}</a>
							</div>
							<div class="news-author">
								<spring:message code="author" />
								: ${elem.author.name}
							</div>
							<div class="news-date">
								<spring:message code="date" />
								: ${elem.news.creationDate.toLocalDate()}
							</div>
						</div>
						<div class="news-content">${elem.news.shortText}</div>
						<div class="news-additional">

							<div class="news-action">
								<sf:form action="${deleteUrl}" method="POST">
									<input type="text" name="idNews" value="${elem.news.id}"
										hidden="true" />
									<input type="text" name="page" value="${cur_page}"
										hidden="true" />
									<button class="news delete-news" type="submit">
										<spring:message code="delete" />
									</button>
								</sf:form>
							</div>
							<div class="news-action">
								<a class="news edit-news"
									href="<c:url value='/news/view/edit/${elem.news.id}'/>"><spring:message
										code="edit" /></a>
							</div>
							<div class="news-action">
								<a class="news comments"
									href="<c:url value='/news/view/${elem.news.id}#comments'/>"><spring:message
										code="comments" />(${fn:length(elem.comments)})</a>
							</div>
							<c:if test="${not empty elem.tags}">
							<div class="news-tags">
								<c:forEach var="tag" items="${elem.tags}"> #${tag.tag}</c:forEach>
							</div>
							</c:if>

						</div>
					</div>
				</c:forEach>
			</c:otherwise>
		</c:choose>
		
		<div class="pagination-container">

			<ul class="pagination">
				<c:if test="${max_page ne 1}">
					<c:if test="${left_switch != null}">

						<li><a href="<c:url value='/news?page=${left_switch}'/>">«</a></li>
					</c:if>
					<c:forEach begin="${min_page}" end="${max_page}" var="val">
						<c:choose>
							<c:when test="${cur_page == val}">
								<li><a href="<c:url value='/news?page=${val}'/>"
									class="active">${val}</a></li>
							</c:when>
							<c:otherwise>
								<li><a href="<c:url value='/news?page=${val}'/>">${val}</a></li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
					<c:if test="${right_switch != null}">
						<li><a href="<c:url value='/news?page=${right_switch}'/>">»</a></li>
					</c:if>

				</c:if>

			</ul>
		</div>
	</div>

</div>
