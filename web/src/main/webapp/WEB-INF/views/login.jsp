<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<div class="login-card">
	<h1><spring:message code="log-in" /></h1>
	<br>
	 <c:url var="loginUrl" value="/login/check" />
	<form action='${loginUrl}' method='POST'>
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
			<c:if test="${not empty param.login_error}">
			<p class="warning"><spring:message code="user.invalid" /></p>
			</c:if>
		<p>
			<input minlength="3" maxlength="30" type="text" name="username" placeholder="<spring:message code="username" />" required> <input
				type="password" name="password" placeholder="<spring:message code="password" />" required> <input
				type="submit" name="login"  minlength="3" maxlength="30" class="login login-submit" value="<spring:message code="login" />">            
		
	</form>
</div>
