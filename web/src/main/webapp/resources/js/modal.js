var conf_modal = document.getElementById('conformation-modal');
var edit_modal = document.getElementById('edit-modal');
var latestForm = null;


//When the user clicks on <span> (x), close the conf_modal
$('#close-conf_modal').on( "click", function() {
	 conf_modal.style.display = "none";
})

$('#close-edit_modal').on( "click", function() {
	edit_modal.style.display = "none";
})

//When the user clicks anywhere outside of the conf_modal, close it
window.onclick = function(event) {
 if (event.target == conf_modal) {
     conf_modal.style.display = "none";
 }

	if (event.target == edit_modal) {
		edit_modal.style.display = "none";
	}
 }


$(function () {

	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});
	});


$("#confirm").on('click', function(e) {
	e.preventDefault();
	latestForm.submit();

});

$("#edit").submit( function(e) {
	
	e.preventDefault();
	var url = $("#edit").attr('action');
	var form = $("#edit").serializeArray();
	var formObject = {};
	$.each(form,
			function(i, v) {
				formObject[v.name] = v.value;
			});


	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : url,
		data : JSON.stringify(formObject),
		timeout : 10000,
		success : function(data) {
			edit_modal.style.display = "none";
			window.location.reload();
		},
		error : function(e) {
			$('#edit-message').text(e.responseText);
		}
	});

});


$(".delete-comment").on('click', function(e){
	e.preventDefault();
	latestForm = $(this).closest('form');
	//var id = $(this).siblings('input[name="id"]').attr('value');
	var comment = $(this).closest('.comment').find('.comment-text').text();
	//$('#delete-form').find('input[name="id"]').attr('value', id);
	$(conf_modal).find('#conf-message').text(comment);
	conf_modal.style.display = "block";
})

$(".delete-news").on('click', function(e){
	e.preventDefault();
	latestForm = $(this).closest('form');
	var title = $(this).closest('.news-additional').siblings('.news-info').find('.news-title a').text()
	$(conf_modal).find('#conf-message').text(title);
	conf_modal.style.display = "block";
})

$(".delete-tag").on('click', function(e){
	e.preventDefault();
	latestForm = $(this).closest('form');
	var tagText = $(this).closest('li').find('p:first-child').text()
	$(conf_modal).find('#conf-message').text(tagText);
	conf_modal.style.display = "block";
})

$(".delete-author").on('click', function(e){
	e.preventDefault();
	latestForm = $(this).closest('form');
	var authText = $(this).closest('li').find('p:first-child').text();
	$(conf_modal).find('#conf-message').text(authText);
	conf_modal.style.display = "block";
})

$(".edit-item").on('click', function(e){
	e.preventDefault();
	var id = $(this).siblings('form').find('input[name="id"]').attr('value');
	var edit_url = $('#edit-url').text();
	var form = $(edit_modal).find('form');
	$(form).attr('action', edit_url);
	$(form).find('input[name="id"]').attr('value', id);
	var text = $(this).closest('li').find('p:first-child').text();
	$(form).find('input[name="name"]').attr('value', text);
	$(form).find('input[name="tag"]').attr('value', text);

	$('#edit-message').text('');

	edit_modal.style.display = "block";
})

$("#create").on('click', function(e){
	e.preventDefault();
	var create_url = $('#create-url').text();
	var form = $(edit_modal).find('form');
	$(form).attr('action', create_url);
	$(form).find('input[name="id"]').attr('value', '');
	
	$(form).find('input[name="name"]').attr('value', '');
	$(form).find('input[name="tag"]').attr('value', '');


	$('#edit-message').text('');

	edit_modal.style.display = "block";
})


$("#cancel").on('click', function(e){
	 conf_modal.style.display = "none";
})


