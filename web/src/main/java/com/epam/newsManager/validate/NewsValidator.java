package com.epam.newsManager.validate;

import com.epam.newsManagerApp.domain.Author;
import com.epam.newsManagerApp.domain.News;
import com.epam.newsManagerApp.domain.Tag;
import com.epam.newsManagerApp.service.AuthorService;
import com.epam.newsManagerApp.service.TagService;
import com.epam.newsManagerApp.service.exception.ServiceException;
import com.epam.newsManagerApp.transfer.NewsTransferObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class NewsValidator implements Validator {

    @Autowired
    TagService tagService;

    @Autowired
    AuthorService authorService;

    @Override
    public boolean supports(Class<?> paramClass) {
        return NewsTransferObject.class.equals(paramClass);
    }

    @Override
    public void validate(Object obj, Errors errors) {

        NewsTransferObject newsTO = (NewsTransferObject) obj;

        News news = newsTO.getNews();


        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.title", "value.notNull");

        String title = news.getTitle().trim();

        if (title != null) {


            if (title.length() < 3 || title.length() > 30) {
                errors.rejectValue("news.title", "title.required");
            }

        }

        news.setTitle(title);

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.shortText", "value.notNull");

        String shortText = news.getShortText().trim();

        if (shortText != null) {

            if (shortText.length() < 10 || shortText.length() > 100) {
                errors.rejectValue("news.shortText", "shortText.required");
            }
            

        }
        
        news.setShortText(shortText);
    
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.fullText", "value.notNull");

        String fullText = news.getFullText().trim();

        if (fullText != null) {


            if (fullText.length() < 50 || fullText.length() > 2000) {
                errors.rejectValue("news.fullText", "fullText.required");
            }
        }
        
        news.setFullText(fullText);
        
        if (news.getCreationDate() != null && news.getCreationDate().isAfter(LocalDateTime.now())) {
            errors.rejectValue("news.creationDate", "creationDate.required");
        }

        Author author = newsTO.getAuthor();

        if (author == null || author.getName() == null || author.getName().isEmpty()) {
            errors.rejectValue("author", "author.required");
        } else {

            try {
                if (authorService.getId(author)==null) {
                    errors.rejectValue("author", "author.not_exist");
                }
            } catch (ServiceException e) {
                errors.rejectValue("author", "author.exception");
            }
        }

        List<Tag> tagList = newsTO.getTags();

        if (tagList != null && !tagList.isEmpty()) {
            try {
                if (!tagService.existAll(tagList)) {
                    errors.rejectValue("tags", "tags.not_exist");
                }
            } catch (ServiceException e) {
                errors.rejectValue("tags", "tag.exception");
            }
        }


    }

}
