package com.epam.newsManager.validate;

import com.epam.newsManagerApp.domain.Author;
import com.epam.newsManagerApp.service.AuthorService;
import com.epam.newsManagerApp.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Artsiom_Yurhevich on 7/28/2016.
 */
@Component
public class AuthorValidator implements Validator {

    @Autowired
    AuthorService authorService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Author.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Author author = (Author) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "value.notNull");

        String name = author.getName().trim();
        
        
        if(name!=null){
            if (name.length() < 3 || name.length() > 30) {
                errors.rejectValue("name", "author.required");
            }
        }

        author.setName(name);
        
        try {
            if(authorService.getId(author)!=null){
                errors.rejectValue("name", "author.alreadyExist");
            }
        } catch (ServiceException e) {
            errors.rejectValue("name", "author.exception");
        }


    }
}
