package com.epam.newsManager.validate;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.epam.newsManagerApp.domain.Author;
import com.epam.newsManagerApp.domain.Comment;

@Component
public class CommentValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		  return Comment.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		
		   ValidationUtils.rejectIfEmptyOrWhitespace(errors, "comment", "value.notNull");

		    Comment comment = (Comment)obj;
		   
	        String text = comment.getComment().trim();
	       

	        if (text != null) {

	            if (text.length() > 100 || text.length() < 3) {
	                errors.rejectValue("comment", "comment.required");
	            }

	        }
	        
	        comment.setComment(text);
	        
		
	}
	

}
