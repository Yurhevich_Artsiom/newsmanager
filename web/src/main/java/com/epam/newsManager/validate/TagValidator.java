package com.epam.newsManager.validate;

import com.epam.newsManagerApp.domain.Tag;
import com.epam.newsManagerApp.service.TagService;
import com.epam.newsManagerApp.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Artsiom_Yurhevich on 7/28/2016.
 */

@Component
public class TagValidator implements Validator {

    @Autowired
    TagService tagService;

    @Override
    public boolean supports(Class<?> clazz) {
        return Tag.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        Tag tag = (Tag) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tag", "value.notNull");

        String tagText = tag.getTag().trim();


        if(tagText!=null){
            if (tagText.length() < 2 || tagText.length() > 30) {
                errors.rejectValue("tag", "tag.required");
            }
        }

        tag.setTag(tagText);
        
        try {
            if(tagService.getId(tag)!=null){
                errors.rejectValue("tag", "tag.alreadyExist");
            }
        } catch (ServiceException e) {
            errors.rejectValue("tag", "tag.exception");
        }



    }
}
