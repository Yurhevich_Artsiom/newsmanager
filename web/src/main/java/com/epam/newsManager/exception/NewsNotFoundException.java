package com.epam.newsManager.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


public class NewsNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	private final Long id;
	
	public NewsNotFoundException(Long id){
		this.id = id;
	}
}
