package com.epam.newsManager.exception;

/**
 * Created by Artsiom_Yurhevich on 8/1/2016.
 */
public class RestException extends RuntimeException {
    public RestException(Throwable cause) {
        super(cause);
    }
}
