package com.epam.newsManager.exception.handler;

import com.epam.newsManager.exception.RestException;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Artsiom_Yurhevich on 7/28/2016.
 */
@ControllerAdvice
public class ExceptionControllerHandler {

    private static final Logger logger = Logger.getLogger(ExceptionControllerHandler.class);

    private static final String DEFAULT_HTML_ERROR_VIEW = "error";

    @ExceptionHandler(value = {Exception.class, RuntimeException.class})
    public ModelAndView exception(Exception exception, HttpServletRequest request) {
        logger.error("EXCEPTION ", exception);
        ModelAndView modelAndView = new ModelAndView(DEFAULT_HTML_ERROR_VIEW);
        modelAndView.addObject("exception", exception);
        modelAndView.addObject("url", request.getRequestURL());
        return modelAndView;
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({RestException.class})
    public void handleGeneralException(final Exception exception) {
        logger.error(exception);
    }


}
