package com.epam.newsManager.convertor;

import com.epam.newsManagerApp.domain.Tag;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by Artsiom_Yurhevich on 7/27/2016.
 */
public class StringToTag implements Converter<String, Tag> {

    @Override
    public Tag convert(String source) {
        Tag tag = new Tag();
        tag.setTag(source);
        return tag;

    }

}
