package com.epam.newsManager.controller;

import com.epam.newsManager.exception.NewsNotFoundException;
import com.epam.newsManager.utils.PaginationInfo;
import com.epam.newsManager.utils.PaginationUtil;
import com.epam.newsManager.validate.CommentValidator;
import com.epam.newsManager.validate.NewsValidator;
import com.epam.newsManagerApp.domain.Comment;
import com.epam.newsManagerApp.domain.Tag;
import com.epam.newsManagerApp.service.*;
import com.epam.newsManagerApp.service.exception.ServiceException;
import com.epam.newsManagerApp.transfer.NewsTransferObject;
import com.epam.newsManagerApp.transfer.search.NewsSearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import java.time.LocalDateTime;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Artsiom_Yurhevich on 7/14/2016.
 */

@Controller
@SessionAttributes("search")
@RequestMapping("/news")
public class NewsController {

	private final String SEARCH_CRITERIA = "search";

	@Autowired
	NewsTOService newsTOService;

	@Autowired
	NewsService newsService;

	@Autowired
	TagService tagService;

	@Autowired
	AuthorService authorService;

	@Autowired
	CommentService commentService;

	@Autowired
	PaginationUtil paginationUtil;

	@Autowired
	private NewsValidator newsValidator;

	@Autowired
	private CommentValidator commentValidator;
	
	@Autowired
	private MessageSource messageSource;

	@Value("${news.newsPerPage}")
	Integer newsPerPage;

	@Value("${news.pagesPerPage}")
	Integer pagesPerPage;

	private PaginationInfo paginationInfo;

	@PostConstruct
	public void initPaginationInfo() {
		paginationInfo = new PaginationInfo(newsPerPage, pagesPerPage);
	}

	@InitBinder("newsTO")
	private void initNewsBinder(WebDataBinder binder) {
		binder.setValidator(newsValidator);
	}

	@InitBinder("comment")
	private void initCmtBinder(WebDataBinder binder) {
		binder.setValidator(commentValidator);
	}

	@ModelAttribute
	public void authorsAndTags(Model model) throws ServiceException {
		model.addAttribute("authorList", authorService.getAll());
		model.addAttribute("tagList", tagService.getAll());
	}

	@ModelAttribute
	public void searchCriteria(Model model) throws ServiceException {
		if (!model.containsAttribute(SEARCH_CRITERIA)) {
			model.addAttribute(SEARCH_CRITERIA, new NewsSearchCriteria());
		}
	}

	@RequestMapping(value = { "/list", "" }, method = RequestMethod.GET)
	public ModelAndView viewNewsList(@ModelAttribute("search") NewsSearchCriteria criteria,
			@RequestParam(value = "page", defaultValue = "1") Integer page, Locale locale) throws ServiceException {

		ModelAndView view = new ModelAndView("index");

		criteria.setFrom((page - 1) * paginationInfo.getItemsPerPage());
		criteria.setCount(paginationInfo.getItemsPerPage());

		int countOfRecords = newsService.countNews(criteria);

		paginationUtil.setPagePagination(paginationInfo, page, countOfRecords, view);

		view.addObject("newsList", newsTOService.getNewsTransferObject(criteria));
		view.addObject("active", "news/list");
		view.addObject("title", messageSource.getMessage("page.main", null, locale));

		return view;
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET )
	public String add(Model model, Locale locale) {

		if (!model.containsAttribute("newsTO")) {		
			model.addAttribute("newsTO",  new NewsTransferObject());
		}
		

		model.addAttribute("active", "news/add");
		model.addAttribute("title", messageSource.getMessage("page.add", null, locale));

		return "add";
	}

	@RequestMapping(value = "/post", method = RequestMethod.POST)
	@Secured(value = "ROLE_ADMIN")
	public String postNews(@Validated @ModelAttribute("newsTO") NewsTransferObject newsTO, BindingResult binding,
			RedirectAttributes redirectAttributes)
			throws ServiceException {

	
			if (binding.hasErrors()) {
				redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.newsTO", binding);
				redirectAttributes.addFlashAttribute("newsTO", newsTO);
				return "redirect:/news/add";
			
		
		}

		Long newsId = newsTOService.addNews(newsTO);

		return "redirect:/news/view/" + newsId;
	}

	@RequestMapping(value = "/view/{newsId}", method = RequestMethod.GET)
	public String viewSingle(@PathVariable Long newsId, @ModelAttribute("search") NewsSearchCriteria criteria,
			Model model, Locale locale) throws ServiceException {

		NewsTransferObject newsTO = newsTOService.getNewsTransferObject(newsId);
		if (newsTO == null) {
			throw new NewsNotFoundException(newsId);
		}

		
		model.addAttribute("newsTO", newsTO);
		
		
		model.addAttribute("nextPrev", newsService.nextPrevNewsNumber(newsTO.getNews().getId(), criteria));

		// model.addAllAttribute("prevId",
		// newsTOService.getNearbyNewsId(newsTO.getNews().getId(), criteria));
		//
		if (!model.containsAttribute("comment")) {
			model.addAttribute("comment", new Comment());
		}

		model.addAttribute("title", messageSource.getMessage("page.view", null, locale));

		return "news";

	}

	@RequestMapping(value = "/view/edit/{newsId}", method = RequestMethod.GET)
	public String viewUpdateNews(@PathVariable Long newsId, Model model, Locale locale) throws ServiceException {

	
		if (!model.containsAttribute("newsTO")) {
			NewsTransferObject newsTO = newsTOService.getNewsTransferObject(newsId);
			
			if (newsTO == null) {
				throw new NewsNotFoundException(newsId);
			}
			
			model.addAttribute("newsTO",  newsTO);
		}
		
		model.addAttribute("title", messageSource.getMessage("page.edit", null, locale));


		return "edit";

	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	@Secured(value = "ROLE_ADMIN")
	public String edit(@Validated @ModelAttribute("newsTO") NewsTransferObject newsTO, BindingResult binding,
			RedirectAttributes redirectAttributes) throws ServiceException {

			if (binding.hasErrors()) {
				redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.newsTO", binding);
				redirectAttributes.addFlashAttribute("newsTO", newsTO);
				return "redirect:/news/view/edit/" + newsTO.getNews().getId();
			}
		

		newsTOService.editNews(newsTO);

		return "redirect:/news/view/" + newsTO.getNews().getId();
	}

	@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such news")
	@ExceptionHandler(NewsNotFoundException.class)
	public void newsNotFound() {
	}

	@RequestMapping(value = "/comment/post", method = RequestMethod.POST)
	public String postComment(@Validated @ModelAttribute("comment") Comment comment, BindingResult binding,
			RedirectAttributes redirectAttributes) throws ServiceException {

		if (binding.hasErrors()) {
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.comment", binding);
			redirectAttributes.addFlashAttribute("comment", comment);
			return "redirect:/news/view/" + comment.getIdNews()+"#new";
		}

		comment.setCreationDate(LocalDateTime.now());

		commentService.addComment(comment);

		return "redirect:/news/view/" + comment.getIdNews()+"#new";
	}

	// @ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = "/comment/delete", method = RequestMethod.POST)
	// public String deleteComment(@RequestBody Map<String, String> input)
	// throws ServiceException {
	// public String deleteComment(@RequestParam Comment comment) throws
	// ServiceException {
	public String deleteComment(@RequestParam("idComment") Long idComment, @RequestParam("idNews") Long idNews)
			throws ServiceException {

		commentService.deleteComment(idComment);

		return "redirect:/news/view/" + idNews;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String deleteNews(@RequestParam("idNews") Long idNews,
			@RequestParam(value = "page", defaultValue = "1") Integer page, RedirectAttributes redirectAttributes)
			throws ServiceException {

		newsTOService.deleteNews(idNews);

		redirectAttributes.addAttribute("page", page);

		return "redirect:/news/list";
	}

	
	@RequestMapping(value = "/orderNumber", method = RequestMethod.GET)
	public String viewByOrderNumber(@RequestParam("orderNumber") Long orderNum, @ModelAttribute("search") NewsSearchCriteria criteria) throws ServiceException{
		
		Long idNews = newsService.getIdByOrderNum(criteria, orderNum);
		
		if(idNews==null){
			throw new NewsNotFoundException(idNews);
		}
		
		return "redirect:/news/view/" + idNews;
		
	}
}