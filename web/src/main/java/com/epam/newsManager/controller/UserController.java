package com.epam.newsManager.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {

	@Autowired
	private MessageSource messageSource;

	@RequestMapping("/login")
	public String login(Model model, Locale locale) {
		model.addAttribute("title", messageSource.getMessage("page.login", null, locale));
		return "login";
	}

}
