package com.epam.newsManager.controller;

import com.epam.newsManager.exception.RestException;
import com.epam.newsManager.utils.PaginationInfo;
import com.epam.newsManager.utils.PaginationUtil;
import com.epam.newsManager.validate.AuthorValidator;
import com.epam.newsManagerApp.domain.Author;
import com.epam.newsManagerApp.service.AuthorService;
import com.epam.newsManagerApp.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping("/author")
public class AuthorController {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    AuthorService authorService;

    @Autowired
    PaginationUtil paginationUtil;

    @Autowired
    private AuthorValidator authorValidator;

    @Value("${authors.authorsPerPage}")
    Integer authorsPerPage;

    @Value("${authors.pagesPerPage}")
    Integer pagesPerPage;

    PaginationInfo paginationInfo;

    @PostConstruct
    public void initPaginationInfo() {
        paginationInfo = new PaginationInfo(authorsPerPage, pagesPerPage);
    }

    @InitBinder("author")
    private void initAuthorBinder(WebDataBinder binder) {
        binder.setValidator(authorValidator);
    }


    @RequestMapping(value = {"/list", ""}, method = RequestMethod.GET)
    public ModelAndView viewAuthorList(@RequestParam(value = "page", defaultValue = "1") Integer page, Locale locale) throws ServiceException {

        ModelAndView view = new ModelAndView("authors");

        Integer from = (page - 1) * paginationInfo.getItemsPerPage();
        Integer count = paginationInfo.getItemsPerPage();

        int countOfRecords = authorService.countAll();

        paginationUtil.setPagePagination(paginationInfo, page, countOfRecords, view);

        view.addObject("authorList", authorService.get(from, count));
        view.addObject("author", new Author());
        view.addObject("active", "author/list");
        view.addObject("title", messageSource.getMessage("page.authors", null, locale));
        return view;

    }


    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String deleteAuthor(@RequestParam("id") Long idAuthor,
                            @RequestParam(value = "page", defaultValue = "1") Integer page, RedirectAttributes redirectAttributes)
            throws ServiceException {


        Author author = new Author();
        author.setId(idAuthor);
        author.setExpired(LocalDateTime.now());

        authorService.setExpired(author);

        redirectAttributes.addAttribute("page", page);

        return "redirect:/author/list";
    }


    @RequestMapping(value = "/edit", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
    public ResponseEntity<String> editAuthor(@Validated @RequestBody Author author, BindingResult binding, Locale locale) throws RestException
    {
         if (binding.hasErrors()) {

            FieldError fieldError =  binding.getFieldError("name");

            String message = messageSource.getMessage(fieldError, locale);

            return new ResponseEntity<>(message, HttpStatus.CONFLICT);
        }

        try {
            authorService.editAuthor(author);
        } catch (ServiceException e) {
            throw new RestException(e);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
    public ResponseEntity<String> createAuthor(@Validated @RequestBody Author author, BindingResult binding, Locale locale) throws RestException
    {
        if (binding.hasErrors()) {

            FieldError fieldError =  binding.getFieldError("name");

            String message = messageSource.getMessage(fieldError, locale);

            return new ResponseEntity<>(message, HttpStatus.CONFLICT);
        }

        try {
            ///!! create
            authorService.getOrCreate(author);
        } catch (ServiceException e) {
            throw new RestException(e);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }


}
