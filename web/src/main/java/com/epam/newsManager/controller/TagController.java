package com.epam.newsManager.controller;

import java.util.Locale;

import javax.annotation.PostConstruct;

import com.epam.newsManager.exception.RestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.epam.newsManager.utils.PaginationInfo;
import com.epam.newsManager.utils.PaginationUtil;
import com.epam.newsManager.validate.AuthorValidator;
import com.epam.newsManager.validate.TagValidator;
import com.epam.newsManagerApp.domain.Author;
import com.epam.newsManagerApp.domain.Tag;
import com.epam.newsManagerApp.service.TagService;
import com.epam.newsManagerApp.service.exception.ServiceException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/tag")
public class TagController {

	@Autowired
	private MessageSource messageSource;

	  @Autowired
	    TagService tagService;

	    @Autowired
	    PaginationUtil paginationUtil;

	    @Autowired
	    private TagValidator tagValidator;

	    @Value("${tags.tagsPerPage}")
	    Integer tagsPerPage;

	    @Value("${tags.pagesPerPage}")
	    Integer pagesPerPage;

	    PaginationInfo paginationInfo;

	    @PostConstruct
	    public void initPaginationInfo() {
	        paginationInfo = new PaginationInfo(tagsPerPage, pagesPerPage);
	    }

	    @InitBinder("tag")
	    private void initTagBinder(WebDataBinder binder) {
	        binder.setValidator(tagValidator);
	    }
	    

	    @RequestMapping(value = {"/list", ""}, method = RequestMethod.GET)
	    public ModelAndView viewTagList(@RequestParam(value = "page", defaultValue = "1") Integer page, Locale locale) throws ServiceException {

	        ModelAndView view = new ModelAndView("tags");

	        Integer from = (page - 1) * paginationInfo.getItemsPerPage();
	        Integer count = paginationInfo.getItemsPerPage();

	        int countOfRecords = tagService.countAll();

	        paginationUtil.setPagePagination(paginationInfo, page, countOfRecords, view);

	        view.addObject("tagList", tagService.get(from, count));
	        view.addObject("tag", new Tag());
	        view.addObject("active", "tag/list");
	        view.addObject("title", messageSource.getMessage("page.tags", null, locale));

	        return view;

	    }


	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String deleteTag(@RequestParam("id") Long idTag,
							 @RequestParam(value = "page", defaultValue = "1") Integer page, RedirectAttributes redirectAttributes)
			throws ServiceException {

		tagService.deleteTag(idTag);

		redirectAttributes.addAttribute("page", page);

		return "redirect:/tag/list";
	}


	@RequestMapping(value = "/edit", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public ResponseEntity<String> editAuthor(@Validated @RequestBody Tag tag, BindingResult binding, Locale locale) throws RestException
	{
		if (binding.hasErrors()) {

			FieldError fieldError =  binding.getFieldError("tag");

			String message = messageSource.getMessage(fieldError, locale);

			return new ResponseEntity<>(message, HttpStatus.CONFLICT);
		}

		try {
			tagService.editTag(tag);
		} catch (ServiceException e) {
			throw new RestException(e);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
	public ResponseEntity<String> createAuthor(@Validated @RequestBody Tag tag, BindingResult binding, Locale locale) throws RestException
	{
		if (binding.hasErrors()) {

			FieldError fieldError =  binding.getFieldError("tag");

			String message = messageSource.getMessage(fieldError, locale);

			return new ResponseEntity<>(message, HttpStatus.CONFLICT);
		}

		try {
			///!! create
			tagService.getOrCreate(tag);
		} catch (ServiceException e) {
			throw new RestException(e);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

}
