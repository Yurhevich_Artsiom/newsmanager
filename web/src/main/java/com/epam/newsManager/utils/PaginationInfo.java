package com.epam.newsManager.utils;

/**
 * Created by Artsiom_Yurhevich on 8/2/2016.
 */

//@Component
//@Scope(value="prototype")
public class PaginationInfo {

    private Integer itemsPerPage;
    private Integer pagesPerPage;

    public PaginationInfo(Integer itemsPerPage, Integer pagesPerPage) {
        this.itemsPerPage = itemsPerPage;
        this.pagesPerPage = pagesPerPage;
    }

    public Integer getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public Integer getPagesPerPage() {
        return pagesPerPage;
    }

    public void setPagesPerPage(Integer pagesPerPage) {
        this.pagesPerPage = pagesPerPage;
    }
}
