package com.epam.newsManager.utils;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;


@Component
public class PaginationUtil {

	private static final String MIN_PAGE = "min_page";
	private static final String MAX_PAGE = "max_page";
	private static final String CUR_PAGE = "cur_page";
	private static final String LEFT_SWITCH = "left_switch";
	private static final String RIGHT_SWITCH = "right_switch";

	public void setPagePagination(PaginationInfo info, int currentPage, int countAll, ModelAndView view) {

		Integer ordersPerPage=info.getItemsPerPage();
		Integer pagesPerPage=info.getPagesPerPage();

		int maxPage;
		int minPage;

		int totalNumberOfPages = (int) Math.ceil(countAll * 1.0 / ordersPerPage);


		if ((float) currentPage % pagesPerPage == 0.0) {
			maxPage = currentPage;
			minPage = currentPage - pagesPerPage + 1;
		} else {
			maxPage = currentPage + (pagesPerPage - currentPage % pagesPerPage);
			minPage = maxPage - pagesPerPage + 1;
		}

		if ( maxPage > totalNumberOfPages) {
			maxPage = totalNumberOfPages;
		}

		view.addObject(MAX_PAGE, maxPage);
		view.addObject(MIN_PAGE, minPage);
		view.addObject(CUR_PAGE, currentPage);

		if (maxPage < totalNumberOfPages) {
			view.addObject(RIGHT_SWITCH, maxPage + 1);
		}

		if (minPage != 1) {
			view.addObject(LEFT_SWITCH, minPage - 1);
		}
	}

}
